"""Remove MWEs that have some kind of overlap with another, longer, MWE.
"""

import sys
import argparse


def main(input_stream, keep_everything=False):
    mwes = []
    for line in input_stream:
        parts = line.strip().split("\t")
        parts[-1] = parts[-1].split()
        mwes.append(parts)

    removed = []
    kept = []
    id_list = [set(mwe[1]) for mwe in mwes]
    nth = 0
    while nth < len(mwes):
        item = mwes[nth]
        item[-1] = " ".join(item[-1])
        ids = id_list.pop(nth)
        if (
            not any(element.issuperset(ids) for element in id_list)
            and not any(len(element.intersection(ids)) > 0 and len(element) > len(ids) for element in id_list)
        ):
            kept.append(item)
        else:
            removed.append(item)
        id_list.insert(nth, ids)
        nth += 1

    if keep_everything:
        for item in kept:
            print("\t".join(item))
        for item in removed:
            print("\t".join(item), file=sys.stderr)
    else:
        for item in kept:
            print("\t".join(item[-2:]))
        for item in removed:
            print("\t".join(item[-2:]), file=sys.stderr)


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="Input stream, either file name or standard input (default: sys.stdin)."
    )
    parser.add_argument(
        "-k", "--keep-everything",
        action="store_true",
        help="Activate if you want to keep every information"
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
