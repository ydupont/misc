"""Get sentences where an MWE is according to its fids.
The input file is a trimmed query output file, where each line consists of one
MWE and its fids, separated by a tabulation."""

import argparse
import sys


def main(input_stream, conllfile):
    mwes = []
    for line in input_stream:
        if " out " in line:
            break
        if line.strip().startswith("#"):
            continue
        if "xxx" in line:
            continue
        parts = line.strip().split("\t")
        if len(parts) >= 2:
            parts[1] = parts[1].split()
            mwes.append(parts)
    lines = []
    id2line = {}
    sentid2line = {}
    with open(conllfile, encoding="utf-8") as stream:
        for i, line in enumerate(stream):
            lines.append(line.strip().split("\t"))
            if len(lines[-1]) > 1:
                id2line[lines[-1][0]] = i
                sentid = int(lines[-1][0][1: lines[-1][0].index("F")])
                sentid2line.setdefault(sentid, i)

    for mwe in mwes:
        fids = sorted(mwe[1], key=lambda x: int(x[x.index("F")+1:]))
        fid = fids[0]
        sentid = int(fid[1 : fid.index("F")])
        start = sentid2line[sentid]
        end = sentid2line.get(sentid+1, len(lines)) - 1
        tokens = lines[start : end]
        text = []
        pattern = (mwe[-1] if len(mwe) > 2 else "")
        for token in tokens:
            if token[0] in fids:
                text.append("<{}>".format(token[1]))
            else:
                text.append(token[1])
        print(mwe[0], end="\t")
        print(" ".join(fids), end="\t")
        if pattern:
            print(" ".join(text), end="\t")
            print(pattern)
        else:
            print(" ".join(text))

    #for line in lines:
    #    print("\t".join(line))


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The MWEs to apply."
    )
    parser.add_argument(
        "conllfile",
        help="The input treetagger file."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
