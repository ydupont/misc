import mweu.dpath

class A:
    def __init__(self, cat, lemma, tree):
        self.cat = cat
        self.lemma = lemma
        self.tree = tree

check = mweu.dpath.to_checker1("noun|lemma=kitten|tree~=cnoun_leaf")

print(check(A("noun", "kitten", "59;n:agreement;nc:agreement;cnoun_leaf|number=pl|person=3|time=-|wh=-|sat=+|enum=-|hum=-|gender=masc")))
print(check(A("cnoun", "kitten", "59;n:agreement;nc:agreement;cnoun_leaf|number=pl|person=3|time=-|wh=-|sat=+|enum=-|hum=-|gender=masc")))
print(check(A("noun", "kitten", "59;n:agreement;nc:agreement;robust_cnoun_leaf|number=pl|person=3|time=-|wh=-|sat=+|enum=-|hum=-|gender=masc")))
print(check(A("noun", "kittens", "59;n:agreement;nc:agreement;cnoun_leaf|number=pl|person=3|time=-|wh=-|sat=+|enum=-|hum=-|gender=masc")))
print(check(A("noun", "kitten", "59;n:agreement;nc:agreement;noun_leaf|number=pl|person=3|time=-|wh=-|sat=+|enum=-|hum=-|gender=masc")))
# print(check())
