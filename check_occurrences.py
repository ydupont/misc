"""Transform a list of depxml files (stored in tar.gz) into a single text file that will be one
sentence per line and tokens separated by a space.
"""

import sys


def main(inputfilename, bratfile):
    with open(inputfilename) as input_stream:
        mwes = [l.strip().lower() for l in input_stream.readlines() if l.strip()]
    with open(bratfile + ".ann") as input_stream:
        annlines = [l.strip().lower() for l in input_stream.readlines() if l.strip()]
    with open(bratfile + ".txt") as input_stream:
        text = input_stream.read().lower()
    for mwe in mwes:
        found = False
        for annline in annlines:
            found = mwe in annline
            if found:
                break
        if found:
            continue
        if mwe in text:
            print('warning: "{}" not annotated but in text'.format(mwe))



def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.replace("\n", " "))
    parser.add_argument(
        "inputfilename",
        help="The path to the input files (tar.gz files with .dis.dep.xml files)."
    )
    parser.add_argument(
        "bratfile",
        help="BRAT file to look into."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
