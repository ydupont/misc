import collections


def fill(hierarchy, classname):
    def fill_rec(hierarchy, classname, to_add, seen):
        next_to_add = set()
        for superclass in to_add:
            if superclass not in seen:
                seen.add(superclass)
                hierarchy[classname].add(superclass)
                next_to_add.update(hierarchy.get(superclass, set()))
        if next_to_add:
            return fill_rec(hierarchy, classname, next_to_add, seen)
        else:
            return hierarchy
    return fill_rec(hierarchy, classname, hierarchy.get(classname, set()), set())


superclasses = collections.defaultdict(set)
filename = "/home/yoann/programmation/almanach/frmg/frgram.smg"

with open(filename, encoding="iso-8859-1") as input_stream:
    lines = input_stream.readlines()

classes = []

for nth, line in enumerate(lines):
    if line.startswith("class "):
        classes.append(nth)
classes.append(len(lines))

for i, lineno in enumerate(classes[:-1]):
    class_name = lines[lineno].split()[1]
    nxt_lineno = classes[i + 1]
    for line in lines[lineno + 1: nxt_lineno]:
        line = line.strip()
        if line.startswith("<:"):
            superclass_name = line.strip().split()[-1][:-1]
            superclasses[class_name].add(superclass_name)

#for key in superclasses:
#    hierarchy = fill(superclasses, key)
print("{")
for key, value in sorted(superclasses.items()):
    print("    '{}': {},".format(key, value))
print("}")


def find_common_ancestor(name1, name2):
    def find_common_ancestor_rec(l1, l2, ancestors1, ancestors2):
        ancestors = {item for item in ancestors1 & ancestors2 if not item.startswith('_')}
        if ancestors:
            return ancestors

        new_l1 = []
        for cls in l1:
            ancestors1 |= superclasses[cls]
            new_l1.extend(list(superclasses[cls]))
        if not new_l1:
            return set()
        new_l2 = []
        for cls in l2:
            ancestors2 |= superclasses[cls]
            new_l2.extend(list(superclasses[cls]))
        if not new_l2:
            return set()
        return find_common_ancestor_rec(new_l1, new_l2, ancestors1, ancestors2)

    return find_common_ancestor_rec([name1], [name2], set([name1]), set([name2]))


def find_common_ancestor1(*names):
    def find_common_ancestor_rec(lists, ancestors_list):
        ancestors = {item for item in set.intersection(*ancestors_list) if not item.startswith('_')}
        if ancestors:
            return ancestors

        new_lists = []
        all_empty = True
        for i, lst in enumerate(lists):
            new_l = []
            for cls in lst:
                ancestors_list[i] |= superclasses[cls]
                new_l.extend(list(superclasses[cls]))
            new_lists.append(new_l)
            if new_l:
                all_empty = False
        if all_empty:
            return set()
        return find_common_ancestor_rec(new_lists, ancestors_list)

    return find_common_ancestor_rec([[name] for name in names], [set([name]) for name in names])


c1, c2, c3 = "verb_extraction_topic_active", "verb_extraction_relative", "verb_canonical"
print(c1, c2, find_common_ancestor(c1, c2))
print("=" * 32)
print(c1, c2, c3, find_common_ancestor1(c1, c2, c3))
