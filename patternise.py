"""Find patterns in contextualized+pattern file.
WARNING! MWEs might be slightly different!
example:
- se agir	E1089F14 E1089F15
- clr agir de	E1089F14 E1089F15 E1089F16
"""

import argparse
import sys
import collections


def main(input_stream, contextfile):
    mwes = []
    for line in input_stream:
        if line.strip().startswith("#"):
            continue
        if line.strip().startswith("%"):
            continue
        if "xxx" in line:
            continue
        parts = line.strip().split("\t")
        if len(parts) == 3:
            mwes.append(parts)

    lines = []
    with open(contextfile, "r", encoding="utf-8") as fd:
        lines = [line.strip().split("\t") for line in fd]
    for mwe in mwes:
        mwe_fids = mwe[1]
        found = False
        for line in lines:
            fids = line[1]
            if mwe_fids == fids:  # no modifications
                print("\t".join(mwe + line[-1:]))
                found = True
        if not found:
            print("\t".join(mwe), file=sys.stderr)


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The MWEs to find (MWE + fids + context)."
    )
    parser.add_argument(
        "contextfile",
        help="The file with MWEs (MWE + fids + context + patterns)."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
