"""Compute agreement on comparison file
"""

import argparse
import collections
import sys

def main(filename):
    with open(filename) as input_stream:
        lines = [l.strip().split("\t") for l in input_stream][1:]
    lines = [
        [line[0]] + [int(i) for i in line[1:-1]] + [float(line[-1])]
        for line in lines
    ]

    total = collections.defaultdict(float)
    number = collections.defaultdict(int)
    how_many = collections.defaultdict(int)

    total["base"] = 0.0
    number["base"] = len(lines)
    how_many["base"] = len(lines[0][1:-1])

    total["humans"] = 0.0
    number["humans"] = 0
    how_many["humans"] = how_many["base"] - 1

    total["2 ok"] = 0.0
    number["2 ok"] = 0
    how_many["2 ok"] = how_many["base"]

    total["2 ok (humans)"] = 0.0
    number["2 ok (humans)"] = 0
    how_many["2 ok (humans)"] = how_many["base"] - 1

    for line in lines:
        agree = line[-1]
        total["base"] += agree
        if line[2:-1].count(1) > 0:
            total["humans"] += sum(i for i in line[2:-1]) / how_many["humans"]
            number["humans"] += 1
        if line[1:-1].count(1) > 1:
            total["2 ok"] += agree
            number["2 ok"] += 1
        if line[2:-1].count(1) > 1:
            total["2 ok (humans)"] += sum(i for i in line[2:-1]) / how_many["2 ok (humans)"]
            number["2 ok (humans)"] += 1

    print("\\begin{table}")
    print("    \\begin{tabular}{lccc}")
    print("        \\toprule")
    print("        comparison & \\# annotations & total agreement & agreement \\\\")
    print("        \\midrule")
    for key in total:
        print("        {} & {} & {:.4} & {:.4} \\\\".format(key.replace("_", " "), number[key], total[key], total[key] / number[key]))
        # print(key)
        # print("{}: {}".format("           number", number[key]))
        # print("{}: {}".format("  total agreement", total[key]))
        # print("{}: {}".format("average agreement", total[key] / number[key]))
        # print()
    print("        \\bottomrule")
    print("    \\end{tabular}")
    print("\\end{table}")


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "filename",
        help="Name of the comparison file to read."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
