"""Transforms an MWE list to a brat corpus."""

import collections
import argparse
import sys


def is_contig(indices):
    diffs = [indices[i+1] - indices[i] for i in range(len(indices)-1)]
    return all(abs(d) == 1 for d in diffs)


def main(input_stream, conllfile, basename, mode="w", value="MWE", discont=False):
    id2mwe = collections.defaultdict(list)
    for nth, line in enumerate(input_stream, 1):
        if " out " in line:
            break
        if line.strip().startswith("#"):
            continue
        if "xxx" in line:
            continue
        parts = line.strip().split("\t")
        if len(parts) >= 2:
            try:
                fids = sorted(parts[1].split(), key=lambda x: int(x.split("F")[1]))
                sid = int(fids[0][1:].split("F")[0])
                indices = [int(fid.split("F")[1])-1 for fid in fids]
                if not(discont) and is_contig(indices):
                    id2mwe[sid].append([parts[0], sid, fids])
                else:
                    print("not contig: ", indices, file=sys.stderr)
            except (ValueError, IndexError):
                print("at line {}".format(nth), file=sys.stderr)
                raise
    lines = []
    id2line = {}
    sentid2line = {}
    sentences = []
    fids = []
    with open(conllfile, encoding="utf-8") as stream:
        sentence = []
        fid = []
        for i, line in enumerate(stream):
            lines.append(line.strip().split("\t"))
            if len(lines[-1]) > 1:
                id2line[lines[-1][0]] = i
                sentid = int(lines[-1][0][1: lines[-1][0].index("F")])
                sentid2line.setdefault(sentid, i)
                sentence.append(lines[-1][1])
                fid.append(lines[-1][0])
            elif sentence:
                sentences.append(sentence[:])
                fids.append(fid[:])
                del sentence[:]
                del fid[:]

    shift = 0
    nth = 1
    if mode == "a":
        m = 0
        with open(basename + ".ann", "r", encoding="utf-8") as annfile:
            for line in annfile:
                head, *_ = line.strip().split("\t")
                index = int(head[1:])
                m = max(m, index)
        nth = m + 1
    with open(basename + ".txt", "w", encoding="utf-8") as txtfile, \
        open(basename + ".ann", mode, encoding="utf-8") as annfile:
        for i, sentence in enumerate(sentences, 1):
            fid = fids[i-1]
            annots = id2mwe[i]
            for tokens, sid, mwe_fids in annots:
                start = fid.index(mwe_fids[0])
                end = fid.index(mwe_fids[-1])
                boff = sum(len(x) for x in sentence[: start]) + len(sentence[: start])
                eoff = sum(len(x) for x in sentence[: end+1]) + len(sentence[: end+1]) - 1
                span = "{} {}".format(shift + boff, shift + eoff)
                if " ".join(sentence[start: end+1]) == "":
                    print(tokens, mwe_fids)
                annfile.write(
                    "T{}\t{} {}\t{}\n".format(
                        nth,
                        value, span,
                        " ".join(sentence[start: end+1])
                    )
                )
                nth += 1
            text = " ".join(sentence)
            txtfile.write(text + "\n")
            shift += len(text)+1 # LF


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The MWEs to apply."
    )
    parser.add_argument(
        "conllfile",
        help="The input conll file (source corpus with fids)."
    )
    parser.add_argument(
        "basename",
        help="basename of output file."
    )
    parser.add_argument(
        "--mode",
        choices=["w", "a", "x"], default="w",
        help="how to open .ann file."
    )
    parser.add_argument(
        "--value",
        default="MWE",
        help="Value of the MWE annotation."
    )
    parser.add_argument(
        "--discont",
        action="store_true",
        help="Also use discontiguous MWEs."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
