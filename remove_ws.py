import re
import sys

reg = re.compile(r"\|ws=\[.+?\]")

with open(sys.argv[1], "r") as fd:
    for line in fd:
        print(reg.sub("", line.strip()))
