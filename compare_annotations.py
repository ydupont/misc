"""Compare annotations (.ann) files from different sources."""

import argparse
import sys
import pathlib
import collections


def main(files):
    annotation2annotator = collections.defaultdict(set)
    all_annotators = set()
    for filename in files:
        path = pathlib.Path(filename)
        name = path.stem
        all_annotators.add(name)
        with open(filename) as inp:
            for line in inp:
                parts = line.strip().split("\t")
                span = parts[1].split(" ", 1)[1]
                annot = " - ".join([span] + parts[2:])
                annotation2annotator[annot].add(name)
    all_annotators = sorted(all_annotators)
    print("{}\t{}\t{}".format("annotation", "\t".join(all_annotators), "agreement"))
    items = annotation2annotator.items()
    for annotation, annotators in sorted(items, key=lambda x: [int(t) for t in x[0].split()[0:2]]):
        scores_int = [int(annotator in annotators) for annotator in all_annotators]
        scores_str = [str(int(annotator in annotators)) for annotator in all_annotators]
        print("{}\t{}\t{}".format(annotation, "\t".join(scores_str), sum(scores_int) / len(all_annotators)))


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "files", nargs="+",
        help="Reference file."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
