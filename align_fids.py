"""Align old fids with new fids."""

import argparse
import sys
import collections
import re


Sentence = collections.namedtuple("Sentence", ("fids", "words", "POS", "lemmas"))

constant_mwes = [
    ("au-dessus", "à le - dessus"),
    ("la preuve", "la_preuve la_preuve"),
    ("de très près", "de_très_près de_très_près de_très_près"),
    ("quelqu'un", "quelqu'_un quelqu'_un"),
    ("à cause de", "à_cause_de à_cause_de à_cause_de"),
    ("au fond de", "au_fond_de au_fond_de au_fond_de"),
    ("un peu de", "un_peu_de un_peu_de un_peu_de"),
    ("un peu", "un_peu un_peu"),
    ("auprès de", "auprès_de auprès_de"),
    ("autour de", "autour_de autour_de"),
    ("faute de", "faute_de faute_de"),
    ("tout près de", "tout_près_de tout_près_de tout_près_de"),
    ("à le fois", "à_la_fois à_la_fois à_la_fois"),
    ("à travers", "à_travers à_travers"),
    ("sans doute", "sans_doute sans_doute"),
    ("ilimp cll avoir", "il_y_a il_y_a")
]

contrs = [
    (r"\bau\b", "à+le"),
    (r"\baux\b", "à+les"),
    (r"\bdu\b", "de+le"),
    (r"\bdes\b", "de+les")
]


def transform(mwe, reverse=False):
    for a, b in constant_mwes:
        if reverse:
            a, b = b, a
        if mwe == a:
            return b
    return mwe


def subst(mwe, reverse=False):
    for a, b in contrs:
        if reverse:
            a, b = b, a
        mwe = re.sub(a, b, mwe)
    return mwe


def find_sub(sublist, superlist):
    indices = []
    for i in range(len(superlist) - len(sublist) + 1):
        if sublist == superlist[i : i + len(sublist)]:
            indices.append(i)
    return indices


def main(input_stream, conllfile, only_right_sentence=False):
    seen = set()
    mwes = []
    for line in input_stream:
        if line.strip().startswith("#"):
            continue
        if line.strip().startswith("%"):
            continue
        if "xxx" in line:
            continue
        parts = line.strip().split("\t")
        if len(parts) >= 2:
            parts[0] = transform(parts[0])
            parts[0] = parts[0].split()
            parts[1] = parts[1].split()
            if not only_right_sentence and tuple(parts[0]) in seen:
                continue
            seen.add(tuple(parts[0]))
            mwes.append(parts)
    sentences = []
    sentence = Sentence([], [], [], [])
    with open(conllfile, "r", encoding="utf-8") as fd:
        for line in fd:
            line = line.strip()
            if line == "</s>" or (not line and sentence.fids):
                sentences.append(sentence)
                sentence = Sentence([], [], [], [])
            else:
                fid, word, POS, lemma = line.split("\t")
                sentence.fids.append(fid)
                sentence.words.append(word)
                sentence.POS.append(POS)
                sentence.lemmas.append(lemma)
    if sentence.fids:
        sentences.append(sentence)
    found = set()
    unfound = set()
    for mwe in mwes:
        ok = False
        tokens = mwe[0]
        test = 1
        for sentence in sentences:
            test += 1
            if sentence.fids[0].split("F")[0] != mwe[1][0].split("F")[0]:
                continue
            lemmas = sentence.lemmas
            indices = find_sub(tokens, lemmas)
            if not indices:
                indices = find_sub(subst(" ".join(tokens)).split(), lemmas)
            for index in indices:
                print(transform(" ".join(lemmas[index : index+len(tokens)]), reverse=True), end="\t")
                if len(mwes) == 2:
                    print(" ".join(sentence.fids[index : index+len(tokens)]))
                else:
                    print(" ".join(sentence.fids[index : index+len(tokens)]), end="\t")
                    print("\t".join(mwe[2:]))
                ok = True
            if only_right_sentence:
                break
        if ok:
            found.add(tuple(tokens))
        else:
            unfound.add(tuple(tokens))
    for unf in sorted(unfound):
        print(" ".join(unf), file=sys.stderr)
    print(len(unfound), file=sys.stderr)


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The MWEs to apply."
    )
    parser.add_argument(
        "conllfile",
        help="The input treetagger file."
    )
    parser.add_argument(
        "-r", "--only-right-sentence", action="store_true",
        help="Look only in the right sentence (matching fids)."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
