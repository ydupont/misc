"""Filter, modify and output MWEs given:
    - an adjudicated MWE annotation file with:
        -- raw annotation,
        -- ok/out value,
        -- disjunct value,
        -- modify value (if applicable),
    - the raw text from which annotations are derived,

to output a cleaned annotation with only valid MWEs with correct span.
"""

import sys


def main(filename, text_filename):
    # text_filename = 
    with open(text_filename) as input_stream:
        text = input_stream.read()


    # filename = 
    # cols = ('annotation', '@mydep', 'mama', 'mumu', 'yaya', 'yoyo', 'agreement', 'modifier ', 'modify', 'pas ok', 'pourquoi ', 'disjunct')
    new_cols = ('start', 'end', 'text', '@mydep', 'mama', 'mumu', 'yaya', 'yoyo', 'agreement', 'modifier ', 'modify', 'pas ok', 'pourquoi ', 'disjunct') # TODO: add lemma
    modify_col = 'modify'
    out_col = 'pas ok'
    OUT_VALUE = 'out'
    disjunct_col = 'disjunct'
    DISJUNCT_VALUE = 'disjunct'
    with open(filename) as input_stream:
        header = next(input_stream)
        print('\t'.join(new_cols))
        cols = header.strip('\r\n').split('\t')
        # we have "raw" file with both invalid and merged MWEs with spans to be modified
        # we only handle contiguous validated MWEs : filter out incorrect and disjunct
        i = 1
        for line in input_stream:
            i += 1
            line = line.strip('\r\n')
            data = dict(zip(cols, line.split('\t')))
            # ~ print('='*32)
            # ~ print(cols)
            # ~ print(line.split('\t'))
            # ~ print(line)
            # ~ print(data)
            # ~ print('='*32)
            if data[disjunct_col]:
                continue
            if data[modify_col]:
                modify_to = data[modify_col]
                start, end = [int(token) for token in data['annotation'].split(' - ', 1)[0].split()]
                textspan = text[start: end]
                try:
                    index = textspan.index(modify_to)
                except ValueError:
                    print(f"ERROR: line {i} - annotation='{data['annotation']}' - start,end={start},{end} - base_text='{textspan}' - modify='{modify_to}'", file=sys.stderr)
                    continue
                new_start = start + index
                new_end = new_start + len(modify_to)
                new_annotation = f'{new_start} {new_end} - {modify_to}'
                assert text[new_start: new_end] == modify_to
                data['annotation'] = new_annotation
                data['start'] = str(start)
                data['end'] = str(end)
                data['text'] = modify_to
                # data['lemma'] = 
                print('\t'.join([data[col] for col in new_cols]))
            else:
                span, textspan = data['annotation'].split(' - ', 1)
                start, end = [int(token) for token in span.split()]
                data['start'] = str(start)
                data['end'] = str(end)
                data['text'] = textspan
                # data['lemma'] = 
                print('\t'.join([data[col] for col in new_cols]))


def parse_cl(argv=None):
    import argparse


    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "filename",
        help="Input annoation filename (tabular)."
    )
    parser.add_argument(
        "text_filename",
        help="Input text filename (.txt)."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
