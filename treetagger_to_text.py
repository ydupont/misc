import sys


def main(input_stream, output_stream):
    begline = True
    for line in input_stream:
        parts = line.strip().split()
        if parts[0] == "</s>":
            output_stream.write("\n")
            begline = True
        else:
            if not begline:
                output_stream.write(" ")
            output_stream.write(parts[0])
            begline = False


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser("Remove features from patterns")
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The path to the input file."
    )
    parser.add_argument(
        "output_stream",
        nargs="?",
        type=argparse.FileType("w"), default=sys.stdout,
        help="The path to the input file."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
