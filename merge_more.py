def common_prefix(left, right):
    for i, item in enumerate(zip(left, right)):
        l, r = item
        if l != r:
            return i
    return i


def common_suffix(left, right):
    for i, item in enumerate(zip(left[::-1], right[::-1])):
        l, r = item
        if l != r:
            return i
    return i


filename = "out-merged.txt"
lemmas = []
with open(filename, "r", encoding="utf-8") as input_stream:
    for line in input_stream:
        parts = line.strip().split("\t")
        lemmas.append(parts[-1].split(" / "))

ones = [lemma[0] for lemma in lemmas if len(lemma) == 1]
mores = [lemma for lemma in lemmas if len(lemma) > 1]
for one in ones:
    for more in [
        more
        for more in mores
        if len(more[0].split()) == len(one.split())
    ]:
        for item in more:
            if len(set(item.split()) & set(one.split())) > len(one.split()) / 2:
                print(item, "/", one)
