"""Check elements in common between patterns.
"""

import argparse
import sys


def main(left, right):
    s1 = set()
    with open(left) as input_stream:
        for line in input_stream:
            s1.add(line.strip())
    s2 = set()
    with open(right) as input_stream:
        for line in input_stream:
            s2.add(line.strip())
    for item in sorted(s1 - s2):
        print(item)


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "left",
        help="Reference file."
    )
    parser.add_argument(
        "right",
        help="Comparison file."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
