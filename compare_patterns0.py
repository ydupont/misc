"""Check elements in common between patterns.
"""

import argparse
import collections
import sys


def main(left, right, lc, rc):
    count_l = collections.Counter()
    with open(left) as input_stream:
        for line in input_stream:
            count_l[line.strip().split("\t")[lc].strip()] += 1

    count_r = collections.Counter()
    with open(right) as input_stream:
        for line in input_stream:
            count_r[line.strip().split("\t")[rc].strip()] += 1

    common = set(count_l.keys()) & set(count_r.keys())
    print(common)
    
    l_order = {
        item: count_l[item]
        for item in common
    }
    r_order = {
        item: count_r[item]
        for item in common
    }
    
    print("left:")
    for item, count in sorted(l_order.items(), key=lambda x: -x[1]):
        print("{}\t{}".format(item, count))
    print()
    print("right:")
    for item, count in sorted(r_order.items(), key=lambda x: -x[1]):
        print("{}\t{}".format(item, count))
    

def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "left",
        help="Reference file."
    )
    parser.add_argument(
        "right",
        help="Comparison file."
    )
    parser.add_argument(
        "--lc",
        type=int, default=3,
        help="Pattern column for left (default: %(default)s)."
    )
    parser.add_argument(
        "--rc",
        type=int, default=1,
        help="Pattern column for right (default: %(default)s)."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
