# -*- coding:utf-8 -*-

"""
main method: do_tokenize
usage: python sem_tokenizer_standalone.py <input> <output>
"""

import sys
import re
from contextlib import contextmanager

#
# Useful object(s)
#

class Span(object):
    """
    The Span object.

    Attributes
    ----------
    _lb : int
        the lower bound of a Span.
    _ub : int
        the upper bound of a Span.
    """

    __slots__ = ("_lb", "_ub")

    def __init__(self, lb, ub, length=-1):
        self._lb = (min(lb, ub) if length<0 else lb)
        self._ub = (max(lb, ub) if length<0 else lb+length)

    def __eq__(self, span):
        return self.lb == span.lb and self.ub == span.ub

    def __contains__(self, i):
        try:
            return self._lb <= i and i < self._ub
        except TypeError:
            return (self.lb <= i.lb) and (i.ub <= self.ub)

    def __len__(self):
        return self._ub - self._lb

    def __str__(self):
        return "[{span.lb}:{span.ub}]".format(span=self)

    @property
    def lb(self):
        return self._lb

    @property
    def ub(self):
        return self._ub

    @lb.setter
    def lb(self, lb):
        self._lb = min(lb, self._ub)

    @ub.setter
    def ub(self, ub):
        self._ub = max(ub, self._lb)

#
# regular expressions that will be used as rules for tokenizing
#

# see: http://www.ietf.org/rfc/rfc1738.txt
# URLs recognition. Validating URL is both hard and all urls may not be
# valid when analysing textual information. Hence, validity checking is
# kept to bare minimum, covering being more important.
protocol = "(?:http|ftp|news|nntp|telnet|gopher|wais|file|prospero)"
mailto = "mailto"
url_body = "\\S+[0-9A-Za-z/]"
url = "<?(?:{0}://|{1}:|www\.){2}>?".format(protocol, mailto, url_body)
url_re = re.compile(url, re.I)
# email addresses recognition. See URLs.
localpart_border = "[A-Za-z0-9!#$%&'*+\-/=?^_`{|}~]"
localpart_inside = "[A-Za-z0-9!#$%&'*+\-/=?^_`{|}~.]"
localpart = "{0}{1}*".format(localpart_border, localpart_inside)
subdomain_start = "[A-Za-z]"
subdomain_inside = "[A-Za-z0-9\\-]"
subdomain_end = "[A-Za-z0-9]"
subdomain = "{0}{1}*{2}".format(subdomain_start, subdomain_inside, subdomain_end)
domain = "{0}(?:\\.{1})*".format(subdomain, subdomain)
email_str = "{0}@{1}".format(localpart, domain)
email_re = re.compile(email_str)

_dots = re.compile(r"(\.{2,})$")
_spaces = re.compile(r"\s+", re.U+re.M)
_word = re.compile(r"^[^\W\d]+$", re.U + re.M)
_number_with_unit = re.compile(u"([0-9][^0-9,.])|([^0-9,.][0-9])")
_atomic = re.compile(r"[;:«»()\[\]{}=+*$£€\\/\"…%€$£]")
_strong_punct = re.compile(r"(\.*[?!]+\.*|\.\.+),?")
_comma_not_number = re.compile(r"(?<=[^0-9]),(?![0-9])", re.U + re.M)
_apostrophe = re.compile(r"(?=['ʼ’])", re.U + re.M)
_clitics = re.compile(r"(-je|-tu|-nous|-vous|(:?-t)?-(:?on|ils?|elles?))\b", re.U + re.I)

# do not tokenize anything that matches any of those regexp
# used to avoid over-tokenization
_forbidden = [
    re.compile(r"\b(dr|me?lles?|mme?s?|mr?s?|st)\.?$", re.U + re.I),  # abbreviations before proper nouns
    re.compile(r"\b(i\.e\.|e\.g\.|c-à-d)", re.U + re.I),  # abbreviations
    _clitics,
    re.compile("^[:;x=],?-?[()DdPp]+$")  # simple smileys
]

# force tokenization of items matching any of those regexp
# used to tokenize somewhat complex items where local rules fail
_force = [
    _clitics,
    url_re,
    email_re
]

#
# methods
#

def word_spans(content):
    """Return spans for words found in content. Note: this method assumes a correct input,
    you need to manually check outside this methods if the buffer ended in the middle of a word.

    Overall process:
    1/ First split input by spaces;
    2/ apply "force" split;
    3/ check for "forbidden" split;
    4/ check for local rules.

    Most the source code in spent in 4/, serious need to formalize how local rules work.
    At each moment, you are at ith token t_i.
    If a token is split, the result can end up with up to 3 subtokens.
    Each of these 3 subtokens will replace t_i and checked individually.
    The list is went through this way until the end is reached.
    """

    # l will hold all the spans [start, end]
    # heavily modified in place with inserts.
    l = [match.span() for match in _spaces.finditer(content)]

    if l:
        l1 = [(l[i][1], l[i+1][0]) for i in range(len(l)-1)]
        if l[0][0] != 0:
            l1.insert(0, (0, l[0][0]))
        if l[-1][1] != len(content):
            l1.append((l[-1][1], len(content)))
    else:
        l1 = [(0, len(content))]

    i = 0
    while i < len(l1):
        span = l1[i]
        text = content[span[0] : span[1]]
        shift = span[0]

        if len(text) == 1:
            i += 1
            continue
        if _word.match(text):
            i += 1
            continue

        # checking for forced tokenization
        found = False
        for force in _force:
            found = force.search(text)
            if found:
                s,e = found.start(), found.end()
                del l1[i]
                l1.insert(i, (shift+e, shift+len(text)))
                l1.insert(i, (shift+s, shift+e))
                if s != 0:
                    l1.insert(i, (shift, shift+s))
                i += 1
                break
        if found:
            continue
        # checking for forbidden tokenization
        for forbidden in _forbidden:
            found = forbidden.match(text)
            if found:
                i += 1
                break
        if found:
            continue
        tmp = []
        # atomic characters, they are always split
        prev = span[0]
        for find in _atomic.finditer(text):
            if prev != shift+find.start():
                tmp.append((prev, shift+find.start()))
            tmp.append((shift+find.start(), shift+find.end()))
            prev = shift+find.end()
        if tmp:
            if prev != span[1]:
                tmp.append((prev, span[1]))
            del l1[i]
            for t in reversed(tmp):
                l1.insert(i, t)
            continue
        # strong puncts, always split
        prev = span[0]
        for find in _strong_punct.finditer(text):
            if prev != shift+find.start():
                tmp.append((prev, shift+find.start()))
            tmp.append((shift+find.start(), shift+find.end()))
            prev = shift+find.end()
        if tmp:
            if prev != span[1]:
                tmp.append((prev, span[1]))
            del l1[i]
            for t in reversed(tmp):
                l1.insert(i, t)
            i += 1
            continue
        # commas
        prev = span[0]
        find = _comma_not_number.search(text)
        if find:
            tmp.extend(
                [
                    (prev, shift+find.start()),
                    (shift+find.start(), shift+find.end()),
                    (shift+find.end(), span[1])
                ]
            )
            prev = shift+find.end()+1
        if tmp:
            del l1[i]
            for t in reversed(tmp):
                l1.insert(i, t)
            continue
        # apostrophes
        prev = span[0]
        for find in _apostrophe.finditer(text):
            tmp.append((prev, shift+find.start()+1))
            prev = shift+find.start()+1
        if prev < span[1]:
            tmp.append((prev, span[1]))
        if len(tmp) > 1:
            del l1[i]
            for t in reversed(tmp):
                l1.insert(i, t)
            continue
        del tmp[:]
        # number with unit
        prev = span[0]
        for find in _number_with_unit.finditer(text):
            tmp.append((prev, span[0]+find.start()+1))
            prev = span[0]+find.start()+1
        if tmp:
            tmp.append((prev, span[1]))
            del l1[i]
            for t in reversed(tmp):
                l1.insert(i, t)
            continue
        # dots and ending commas
        if text and (text[-1] in ".," and not (len(text) == 2 and text[0].isupper())):
            mdots = _dots.search(text)
            length = (len(mdots.group(1)) if mdots else 1)
            if length != len(text):
                tmp = [(span[0], span[1]-length), (span[1]-length, span[1])]
        if tmp:
            del l1[i]
            for t in reversed(tmp):
                l1.insert(i, t)
            continue
        i += 1

    spans = [Span(s[0], s[1]) for s in l1 if s[0] < s[1]]
    spans = [span for span in spans if len(span) > 0]
    return spans


def sentence_bounds(content, token_spans):
    """Return sentence bounds.
    A bound is a (possibly zero-width) span seperating to elements.
    """

    sent_bounds = []  # changed from: sent_bounds = SpannedBounds()
    tokens = [content[t.lb : t.ub] for t in token_spans]

    sent_bounds.append(Span(0, 0))
    for index, span in enumerate(token_spans):
        token = tokens[index]
        if _strong_punct.match(token) or token == "…" or re.match(r"\.+", token):
            if index < len(token_spans)-1 and tokens[index+1][0].islower():
                pass
            elif not(index < len(token_spans)-1 and tokens[index+1] == "»") and not(token.endswith(",")):
                sent_bounds.append(Span(index+1, index+1))
        elif token == "»":
            if index < len(token_spans)-1 and tokens[index+1][0].islower():
                pass
            elif (
                not(index < len(token_spans)-1 and tokens[index+1] == ",")
                and not(index < len(token_spans)-1 and tokens[index+1] == ".")
                and not(index < len(token_spans)-1 and _clitics.search(tokens[index+1]))
                and not(index < len(token_spans)-2 and _clitics.search(tokens[index+2]))
            ):
                sent_bounds.append(Span(index+1, index+1))
        elif index < len(token_spans)-1 and content[span.ub : token_spans[index+1].lb].count("\n") > 1:
            sent_bounds.append(Span(index+1, index+1))
    sent_bounds.append(Span(len(tokens), len(tokens)))

    return sent_bounds


def sentence_spans(content, token_spans):
    """
    creates spans from bounds. sentence_bounds could be copy/pasted into it.
    """

    bounds = sentence_bounds(content, token_spans)
    spans = [Span(bounds[i].ub, bounds[i+1].lb) for i in range(0, len(bounds)-1)]
    spans = [span for span in spans if span.lb != span.ub]
    return spans


#
# methods for tokenizing text
#

@contextmanager
def r_open(source, encoding='utf-8'):
    '''Open in read mode either a file from the filename or a string.'''

    src = None
    try:
        src = open(source, 'r', encoding=encoding, newline='')
    except (TypeError, FileNotFoundError):
        try:
            s = source.decode(encoding)
        except UnicodeDecodeError:
            s = source
        except UnicodeEncodeError:
            s = source
        except AttributeError:
            s = source
        src = StringIO(s)
    finally:
        yield src
        try:
            src.close()
        except Exception:
            pass

def read_chunks(source, size=1024, encoding='utf-8'):
    '''Read a source by chunks of a given size.
    input is decoded, so we read size unicode characters, not size bytes.'''

    with r_open(source, encoding) as input_stream:
        buff = input_stream.read(size)
        while buff:
            yield buff
            buff = input_stream.read(size)


def do_tokenize(source, output_stream=sys.stdout, tokenize=False):
    """Tokenize text from source to target. Both source and target are filenames."""

    tok_rem = ''  # remainder of unsegmented tokens
    for chunk in read_chunks(source, encoding='utf-8'):
        chnk = tok_rem + chunk
        token_spans = word_spans(chnk)
        if not token_spans:
            tok_rem = chnk
            continue
        elif token_spans[-1].ub < len(chnk):
            tok_rem = chnk[token_spans[-1].ub : ]
        elif token_spans[-1].ub == len(chnk):
            tok_rem = chnk[token_spans[-1].lb : ]
            del token_spans[-1]
        else:
            tok_rem = ''
        sent_spans = sentence_spans(chnk, token_spans)
        if sent_spans and sent_spans[-1].ub == len(chnk) or tok_rem:
            lb = sent_spans[-1].lb
            tok_rem = chnk[token_spans[lb].lb : ]
            del sent_spans[-1]
        else:
            tok_rem = ''
        for s_span in sent_spans:
            if tokenize:
                t_spans = token_spans[s_span.lb : s_span.ub]
                output_stream.write(' '.join(chnk[t.lb : t.ub] for t in t_spans) + '\n')
            else:
                fst = token_spans[s_span.lb]
                lst = token_spans[s_span.ub-1]
                output_stream.write(chnk[fst.lb : lst.ub] + '\n')
        del token_spans[:]

    if tok_rem:
        token_spans = word_spans(tok_rem) or [[0, len(tok_rem)]]
    sent_spans = sentence_spans(tok_rem, token_spans)
    for s_span in sent_spans:
        if tokenize:
            t_spans = token_spans[s_span.lb : s_span.ub]
            output_stream.write(' '.join(tok_rem[t.lb : t.ub] for t in t_spans) + '\n')
        else:
            fst = token_spans[s_span.lb]
            lst = token_spans[s_span.ub-1]
            output_stream.write(tok_rem[fst.lb : lst.ub] + '\n')


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(
        description=__doc__.split("\n")[0]
    )
    parser.add_argument(
        "source",
        help="The text to tokenize."
    )
    parser.add_argument(
        "output_stream",
        nargs="?",
        type=argparse.FileType("w"), default=sys.stdout,
        help="The files where to look for MWEs."
    )
    parser.add_argument(
        "-t", "--tokenize",
        action="store_true",
        help="Do we also output tokens?"
    )

    args = parser.parse_args(argv)
    do_tokenize(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
