"""Clean patterns from a dictionary extraction on a corpus.
"""

import collections
import argparse
import sys
import re


def main(inputfile, outputfile, column=1):
    for line in inputfile:
        parts = line.strip().split("\t")
        parts[column] = re.sub(r"\|\S+", "", parts[column])
        outputfile.write("\t".join(parts) + "\n")
    

def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "inputfile",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="Input file."
    )
    parser.add_argument(
        "outputfile",
        nargs="?",
        type=argparse.FileType("w"), default=sys.stdout,
        help="Output file."
    )
    parser.add_argument(
        "-c", "--column",
        type=int, default=1,
        help="The column where patterns are (default: %(default)s)."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
