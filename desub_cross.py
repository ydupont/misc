"""Desubsetize MWEs using a set of MWEs categories instead of a single one."""

import argparse
import sys


def main(files, index=0, tokens_column=-2, fids_column=-1, pattern_column=1):
    mwess = []
    for filename in files:
        with open(filename) as input_stream:
            mwes = [
                [filename] + line.strip().split("\t")
                for line in input_stream
                if line.strip()
            ]
            mwes = [
                [mwe[0], mwe[pattern_column], mwe[tokens_column], mwe[fids_column].split()]
                for mwe in mwes
            ]
            mwess.append(mwes)

    new = []
    for item in mwess[index]:
        add = True
        for mwes in mwess:
            for element in mwes:
                if set(element[-1]).issuperset(set(item[-1])) and set(element[-1]) != set(item[-1]):
                    itm = item[:-1] + [" ".join(item[-1])]
                    elt = element[:-1] + [" ".join(element[-1])]
                    add = False
                    break
            if not add:
                break
        if add:
            new.append(item)

    for filename, pattern, left, right in new:
        print("{}\t{}\t{}".format(left, " ".join(right), pattern))


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "files", nargs="+",
        help="Reference file."
    )
    parser.add_argument(
        "-i", "--index", type=int, default=0,
        help="Which file to choose to desub."
    )
    parser.add_argument(
        "--tokens-column", type=int, default=-2,
        help="column where fids are (default: %(default)s)."
    )
    parser.add_argument(
        "--fids-column", type=int, default=-1,
        help="column where fids are (default: %(default)s)."
    )
    parser.add_argument(
        "--pattern-column", type=int, default=2,
        help="column where patterns are (default: %(default)s)."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
