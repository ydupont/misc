"""Transform depXML files into a single treetagger file.
"""

import sys
import mweu.depxml as depxml


def main(inputfilenames, output_file):
    for inputfilename in inputfilenames:
        sentences = depxml.read_targz(inputfilename)
        for sentence in sentences:
            nodes = sorted(
                sentence.nodes,
                key=lambda x: [int(x) for x in x.cluster.split("_")[-2:]]
            )
            nodes = [("|".join(n.fids), n.form, n.cat, n.lemma) for n in nodes if n.form]
            output_file.write('\n'.join(['\t'.join(n) for n in nodes]) + "\n</s>\n")


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument("inputfilenames", nargs="+", help="The path to the input files.")
    parser.add_argument(
        "output-file",
        type=argparse.FileType("w"), default=sys.stdout,
        help="The path to the output file (default: standard output)."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
