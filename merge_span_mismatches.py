import sys

def spanof(line):
    span = line[: line.index("-")].strip().split()
    return int(span[0]), int(span[1])

def contains(start, end, s, e):
    return start <= s and end >= e

def overlap(start, end, s, e):
    return (
        start <= s and end <= e and end > s
        or start >= s and end <= e and e > start
        or contains(start, end, s, e)
    )

with open(sys.argv[1]) as input_stream, open(sys.argv[2], "w") as output_stream:
    output_stream.write(next(input_stream))
    current = next(input_stream)
    start, end = spanof(current)
    parts = current.strip().split("\t")
    for line in input_stream:
        s, e = spanof(line)
        cont = contains(start, end, s, e)
        ovlp = overlap(start, end, s, e)
        if ovlp:
            start = min(start, s)
            end = max(end, e)
            parts[0] += " /" + line.split("\t")[0][line.index("-") + 1: ]
            sub = line.strip().split("\t")
            for i in range(1, len(parts)-1):
                if sub[i] == "1":
                    parts[i] = "1"
        else:
            parts[0] = "{} {} ".format(start, end) + parts[0][parts[0].index("-") : ]
            parts[-1] = str(sum(int(t) for t in parts[1 : -1]) / (len(parts)-2))
            output_stream.write("\t".join(parts) + "\n")
            parts = line.strip().split("\t")
            start = s
            end = e
    if parts:
        parts[-1] = str(sum(int(t) for t in parts[1 : -1]) / (len(parts)-2))
        output_stream.write("\t".join(parts) + "\n")
