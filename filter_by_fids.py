"""Check elements in common between patterns.
"""

import argparse
import sys


def main(left, right, column):
    fids = set()
    with open(right) as input_stream:
        for line in input_stream:
            fids.add(line.strip().split("\t")[column])

    with open(left) as input_stream:
        for line in input_stream:
            l, r = line.strip().split("\t")
            if r in fids:
                print(line.strip())


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "left",
        help="Reference file."
    )
    parser.add_argument(
        "right",
        help="Comparison file."
    )
    parser.add_argument(
        "--column",
        type=int, default=-3,
        help="fids column (default: %(default)s)."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
