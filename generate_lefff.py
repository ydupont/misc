# le sourire au coin des lèvres	100	cf	[pred="avoir le sourire au coin des lèvres_____1<Suj:cln|sn>",lightverb=avoir]	avoir le sourire au coin des lèvres_____1	Default		%default	
# le taureau par les cornes	100	cf	[pred="attraper le taureau par les cornes_____1<Suj:cln|sn>",synt_head=attraper]	attraper le taureau par les cornes_____1	Default		%default	
# découdre	100	v	[pred="en découdre_____3<Suj:(cln|sn),Obl:(avec-sn)>",@pers,@pseudo-en,cat=v,@W]	découdre_____3	Infinitive	W	%actif	v82
# découdre	100	v	[pred="se découdre_____1<Suj:(cln|sn)>",@pers,@pron,@être,cat=v,@W]	découdre_____1	Infinitive	W	%actif	v82
#
# boîte de conserve	100	nc	[pred="boîte de conserve_____1<Objde:(de-sinf|de-sn),Objà:(à-sinf)>",cat=nc,@fs]	boîte de conserve_____1	Default	fs	%default	nc-2f
# alter ego	100	nc	[pred="alter ego_____1<Objde:(de-sinf|de-sn),Objà:(à-sinf)>",cat=nc]	alter ego_____1	Default		%default	nc-1
# centre ville	100	nc	[pred="centre ville_____1<Objde:(de-sinf|de-sn),Objà:(à-sinf)>",cat=nc,@mp]	centre ville_____1	Default	mp	%default	nc-2m
# kilomètre par heure	100	nc	[pred="kilomètre par heure_____1<Objde:(de-sinf|de-sn),Objà:(à-sinf)>",cat=nc,@ms]	kilomètre par heure_____1	Default	ms	%default	nc-2m
# nouveau venu	100	nc	[pred="nouveau venu_____1<Objde:(de-sinf|de-sn),Objà:(à-sinf)>",cat=nc,@mp]	nouveau venu_____1	Default	mp	%default	nc-eau4
# prix Nobel	100	nc	[pred="prix Nobel_____1<Objde:(de-sinf|de-sn),Objà:(à-sinf)>",cat=nc,@m]	prix Nobel_____1	Default	m	%default	nc-1m
# quartier général	100	nc	[pred="quartier général_____1<Objde:(de-sinf|de-sn),Objà:(à-sinf)>",cat=nc,@ms]	quartier général_____1	Default	ms	%default	0

filename = "out-merged.txt"
data = []
with open(filename, "r", encoding="utf-8") as input_stream:
    for line in input_stream:
        parts = line.strip().split("\t")
        data.append([parts[-2], parts[-1].split(" / ")])


def get_info(cat):
    if cat == "v":
        return {
            "category": "cf",
            "syntax": "<Suj:cln|sn>",
            "category_name": "synt_head",
            "additional": "",
            "morphology": "",
            "morphosyntax": ""
        }
    elif cat == "n":
        return {
            "category": "nc",
            "syntax": "<Objde:(de-sinf|de-sn),Objà:(à-sinf)>",
            "category_name": "cat",
            "additional": "nc,@?",
            "morphology": "?",
            "morphosyntax": "nc-?"
        }


for tree, lemmas in data:
    mwe_cat = tree.split()[1]
    for lemma in lemmas:
        parts = lemma.split()
        print(
            '{entry}'
            '\t{weight}'
            '\t{category}'
            '\t[pred="{predicate}{syntax}",{category_name}={category_value}{additional}]' # _____1
            '\t{predicate}' # _____1
            '\tDefault'
            '\t{morphology}'
            '\t%default'
            '\t{morphosyntax}'
            .format(
                entry=" ".join(parts[1:]),
                weight=100,
                category="cf",  # TODO: cf is only for verbs, adapt to nc, etc.
                predicate="{full}_____{nth_sense}".format(full=" ".join(parts), nth_sense=1),  # adapt to multiple senses
                syntax="<Suj:cln|sn>",
                category_name="synt_head",
                category_value=parts[0],
                additional="",
                morphology="",
                morphosyntax=""
            )
        )
