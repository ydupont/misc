import sys
import re


def main(lexica_file, pmi_file):
    lexica = set()
    with open(lexica_file, "r", encoding="utf-8") as i_s:
        for line in i_s:
            line = line.strip()
            if not line:
                continue
            lexica.add(line)
            cleaned = (
                line
                .replace(" l' ", " le ")
                .replace(" la ", " le ")
                .replace(" du ", " de le ")
                .replace(" d' ", " de ")
                .replace(" au ", " à le ")
            )
            cleaned = re.sub(r"ive\b", "if", cleaned)
            cleaned = re.sub("e$", "", cleaned)
            lexica.add(cleaned)

    with open(pmi_file, "r", encoding="utf-8") as i_s:
        print(next(i_s).strip())
        print(next(i_s).strip())
        for line in i_s:
            if not line.strip():
                continue
            parts = line.strip().split("\t")
            ngram = parts[1]
            if ngram in lexica or ngram.lower() in lexica:
                continue
            print(line.strip())


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser("Remove features from patterns")
    parser.add_argument(
        "lexica_file",
        help="The path to the input file."
    )
    parser.add_argument(
        "pmi_file",
        help="The path to the input file."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
