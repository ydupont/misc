"""Clean patterns from a dictionary extraction on a corpus.
"""

import collections
import argparse
import sys
import re


def main(inputfile, outputfile, matches="", min_support=1, majority_only=False):
    lines = []
    patcount = collections.Counter()
    tokpatcount = collections.defaultdict(collections.Counter)
    tok2pat = {}
    for line in inputfile:
        parts = line.strip().split("\t")
        pattern = parts[3]
        token = parts[2]
        if matches not in pattern:
            continue
        patcount[pattern] += 1
        tokpatcount[token][pattern] += 1
        lines.append(parts)
    if majority_only:
        for token, counter in tokpatcount.items():
            count = counter.most_common(1)[0][1]
            tok2pat[token] = set(
                token
                for (token, value) in counter.most_common()
                if value == count
            )
        lines = [
            line
            for line in lines
            if line[3] in tok2pat[line[2]]
        ]
    if min_support > 1:
        counts = collections.Counter(line[3] for line in lines)
        lines = [
            line
            for line in lines
            if counts[line[3]] >= min_support
        ]
    for line in lines:
        outputfile.write("\t".join(line) + "\n")
    

def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "inputfile",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="Input file."
    )
    parser.add_argument(
        "outputfile",
        nargs="?",
        type=argparse.FileType("w"), default=sys.stdout,
        help="Output file."
    )
    parser.add_argument(
        "-m", "--matches",
        default="",
        help="regex pattern has to match."
    )
    parser.add_argument(
        "-s", "--min-support",
        type=int,
        default=1,
        help="The minimum support of a pattern (default: %(default)s)"
    )
    parser.add_argument(
        "--majority-only",
        action="store_true",
        help="Keep only the majority base pattern for each MWE form."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
