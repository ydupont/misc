import re
import sys


def remove_features(input_stream, features=True, context=True):
    for line in input_stream:
        line = line.strip()
        # removing tree number
        line = re.sub("tree=[0-9]+;", "tree=", line)
        # preparing data for even more things to remove
        parts = line.split("\t")
        tree = parts[-1]
        tree_parts = tree.split()
        tree_parts = [
            part.split("|")
            for part in tree_parts
        ]
        for i in range(len(tree_parts)):
            if "=" in tree_parts[i][-1] and "::context" in tree_parts[i][-1]:
                tree_parts[i][0] += "::context"
                tree_parts[i][-1] = tree_parts[i][-1][: tree_parts[i][-1].index("::context")]
        # removing morphological features
        if features:
            tree_parts = [
                part[0]
                for part in tree_parts
            ]
        else:
            tree_parts = [
                "|".join(part)
                for part in tree_parts
            ]
        tree = " ".join(tree_parts)
        #~ print("-"*32, file=sys.stderr)
        #~ print(tree, file=sys.stderr)
        # removing context
        if context:
            parens = []
            for i in range(len(tree)):
                if tree[i] == "(":
                    parens.append([i, tree.index(" ", tree.index(" ", i + 1) + 1), -1])
                elif tree[i] == ")":
                    for j in reversed(range(len(parens))):
                        if parens[j][-1] == -1:
                            parens[j][-1] = i
                            break
            for fst, nxt, lst in parens:
                if "::context" in tree[fst: nxt]:
                    if fst == 0:
                        index = tree[nxt + 2:].index(" ")
                        tree = "(mwe" + (" " * (nxt - fst - 2 + index)) + tree[nxt + 2 + index: lst]
                    else:
                        tree = tree[:fst] + (" " * (nxt - fst + 2)) + tree[nxt + 2:]
                    #~ print(tree)
        #~ print(tree, file=sys.stderr)
        tree = re.sub("  +", " ", tree)
        #~ print(tree, file=sys.stderr)
        #~ print("-"*32, file=sys.stderr)
        parts.insert(-1, tree)
        print("\t".join(parts))


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser("Remove features from patterns")
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The path to the input file."
    )
    parser.add_argument(
        "-f", "--features", action="store_true",
        help="Remove features"
    )
    parser.add_argument(
        "-c", "--context", action="store_true",
        help="Remove context"
    )
    args = parser.parse_args(argv)

    remove_features(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
