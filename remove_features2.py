import re
import sys


def remove_features(tree_parts):
    return [
        part[0]
        for part in tree_parts
    ]


def remove_context(tree_parts):
    if "::context" in tree_parts[1]:
        tree_parts[-2] = " " * len(tree_parts[-2])
        tree_parts[2] = " " * len(tree_parts[2])
        tree_parts[1] = " " * len(tree_parts[1])
    elif "::context" in tree_parts[2] and not tree_parts[4] == ")":
        tree_parts[-2] = " " * len(tree_parts[-2])
        tree_parts[2] = " " * len(tree_parts[2])
        tree_parts[1] = " " * len(tree_parts[1])
    for index in range(len(tree_parts)):
        item = tree_parts[index]
        if "::context" in item:
            if item.startswith("("): # context on edge
                tree_parts[index] = " " * len(tree_parts[index])
                tree_parts[index + 1] = " " * len(tree_parts[index + 1])
                tree_parts[index + 2] = " " * len(tree_parts[index + 2])
            else: # context on node
                tree_parts[index - 1] = " " * len(tree_parts[index - 1])
                tree_parts[index] = " " * len(tree_parts[index])
                tree_parts[index + 2] = " " * len(tree_parts[index + 1])
    return tree_parts


def main(input_stream, features=True, context=True):
    for line in input_stream:
        line = line.strip()
        # removing tree number
        line = re.sub("tree=[0-9]+;", "tree=", line)
        # preparing data for even more things to remove
        parts = line.split("\t")
        tree = parts[-1]
        tree_parts = tree.split()
        tree_parts = [
            part.split("|")
            for part in tree_parts
        ]
        for i in range(len(tree_parts)):
            if "=" in tree_parts[i][-1] and "::context" in tree_parts[i][-1]:
                tree_parts[i][0] += "::context"
                tree_parts[i][-1] = tree_parts[i][-1][: tree_parts[i][-1].index("::context")]
        basetree_parts = tree_parts[:] # will hold the base tree, with no context and features
        # removing morphological features
        if features:
            tree_parts = remove_features(tree_parts)
        else:
            tree_parts = [
                "|".join(part)
                for part in tree_parts
            ]
        basetree_parts = [
            part[0]
            for part in basetree_parts
        ]
        #~ print("-"*32, file=sys.stderr)
        #~ print(tree, file=sys.stderr)
        # removing context
        if context:
            tree_parts = remove_context(tree_parts)
        basetree_parts = remove_context(basetree_parts)
        spaces = re.compile(" +")
        tree = spaces.sub(" ", " ".join(tree_parts))
        basetree = spaces.sub(" ", " ".join(basetree_parts))
        #~ print(tree, file=sys.stderr)
        parts.insert(-1, basetree)
        parts.insert(-1, tree)
        print("\t".join(parts))


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser("Remove features from patterns")
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The path to the input file."
    )
    parser.add_argument(
        "-f", "--features", action="store_true",
        help="Remove features"
    )
    parser.add_argument(
        "-c", "--context", action="store_true",
        help="Remove context"
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
