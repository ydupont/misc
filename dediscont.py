"""Remove discontinuous MWEs.
"""

import sys
import argparse


def is_contiguous(mwe):
    spans = [
        int(fid[fid.index("F") + 1: ])
        for fid in mwe[-1]
    ]
    return all(
        spans[i+1] - spans[i] == 1
        for i in range(len(spans) - 1)
    )


def main(input_stream, keep_everything=False):
    mwes = []
    for line in input_stream:
        parts = line.strip().split("\t")
        parts[-1] = parts[-1].split()
        mwes.append(parts)
    contig = [
        item
        for item in mwes
        if is_contiguous(item)
    ]
    discont = [
        item
        for item in mwes
        if not is_contiguous(item)
    ]

    for item in contig:
        item[-1] = " ".join(item[-1])
        print("\t".join(item))
    for item in discont:
        item[-1] = " ".join(item[-1])
        print("\t".join(item), file=sys.stderr)


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="Input stream."
    )
    parser.add_argument(
        "-k", "--keep-everything",
        action="store_true",
        help="Activate if you want to keep every information"
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
