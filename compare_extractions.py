"""Check elements in common between patterns.
"""

import argparse
import sys


def main(file1, file2, output_file):
    set1 = set()
    with open(file1) as input_stream:
        for line in input_stream:
            parts = line.strip().split("\t")
            set1.add(parts[-1])
    items = []
    with open(file2) as input_stream:
        for line in input_stream:
            items.append(line.strip().split("\t"))
    for item in items:
        if item[-1] not in set1:
            output_file.write("\t".join(item) + "\n")


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "file1",
        help="Input file."
    )
    parser.add_argument(
        "file2",
        help="Input file."
    )
    parser.add_argument(
        "output_file",
        nargs="?",
        type=argparse.FileType("w"), default=sys.stdout,
        help="Output file."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
