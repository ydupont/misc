import collections
import re
import sys

numbers = re.compile("[0-9]+$")
punct = re.compile(r"[;:«»()\[\]{}/\\\"'?!…%]")
sent2mwes = collections.defaultdict(list)
lemmas = set()
with open(sys.argv[1], "r", encoding="utf-8") as i_s:
    for line in i_s:
        parts = line.strip().split("\t")
        if parts[-2] != parts[-2].strip():
            continue  # some tokens manage to not be verbs...
        if "  " in parts[-2]:
            continue
        if punct.search(parts[-2]):
            continue
        ids = parts[-1].split()
        ids = [int(numbers.findall(x)[0]) for x in ids]
        parts[-1] = ids
        sent2mwes[parts[0]].append(parts)
        lemmas.add(parts[-2])

for key, val in sent2mwes.items():
    val.sort(key=lambda x: x[-1])

for lemma in sorted(lemmas):
    print('_'.join(lemma.split()))
