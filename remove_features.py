import re
import sys

try:
    input_stream = open(sys.argv[1], "r", encoding="utf-8")
except IndexError:
    input_stream = sys.stdin

for line in input_stream:
    line = line.strip()
    # removing tree number
    line = re.sub("tree=[0-9]+;", "tree=", line)
    # preparing data for even more things to remove
    parts = line.split("\t")
    tree = parts[-1]
    tree_parts = tree.split()
    tree_parts = [
        part.split("|")
        for part in tree_parts
    ]
    for i in range(len(tree_parts)):
        if "=" in tree_parts[i][-1] and "::context" in tree_parts[i][-1]:
            tree_parts[i][0] += "::context"
            tree_parts[i][-1] = tree_parts[i][-1][: tree_parts[i][-1].index("::context")]
    # removing morphological features
    tree_parts = [
        part[0]
        for part in tree_parts
    ]
    tree = " ".join(tree_parts)
    #print("-"*32)
    #print(tree)
    # removing context
    search = re.search(r"^\(mwe \S+ \(\S+::context", tree)
    if search:
        start, end = search.span()
        # tree = re.sub(r"^\(mwe \S+ \(\S+::context", "(mwe", tree[:-2].strip()).strip()
        index = tree.rfind(")", 0, tree.rfind(")"))
        tree = tree[: start + 5] + " " * (end - start - 5) + tree[end: index] + " " + tree[index + 1:]
    parens = []
    for i in range(len(tree)):
        if tree[i] == "(":
            parens.append([i, tree.index(" ", tree.index(" ", i + 1) + 1), -1])
        elif tree[i] == ")":
            for j in reversed(range(len(parens))):
                if parens[j][-1] == -1:
                    parens[j][-1] = i
                    break
    for fst, nxt, lst in parens:
        if "::context" in tree[fst: nxt]:
            if fst == 0:
                index = tree[nxt + 2:].index(" ")
                tree = "(mwe" + (" " * (nxt - fst - 2)) + tree[nxt + 2: lst]
            else:
                tree = tree[:fst] + (" " * (nxt - fst + 2)) + tree[nxt + 2:]
    #        print(tree)
    #print("-"*32)
    tree = re.sub("  +", " ", tree)
    parts.insert(-1, tree)
    print("\t".join(parts))

if input_stream != sys.stdin:
    input_stream.close()
