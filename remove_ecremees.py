"""Remove MWEs ecremées from MWE set"""

import sys


def main(input_stream, ecremesfile, column):
    mwes = [
        line.strip().split("\t")
        for line in input_stream
        if line.strip()
    ]
    with open(ecremesfile) as input_stream1:
        ecremes = set(
            line.strip()
            for line in input_stream1
        )

    mwes = [
        mwe
        for mwe in mwes
        if mwe[0] not in ecremes
    ]

    for mwe in mwes:
        print("\t".join(mwe))


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The path to the input file."
    )
    parser.add_argument(
        "ecremesfile",
        help="The path to the ecremées file."
    )
    parser.add_argument(
        "column",
        type=int,
        help="The column to read in pattern file (default: %(default)s)"
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
