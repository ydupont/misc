import sys
import collections

d = collections.defaultdict(set)
for filename in sys.argv:
    with open(filename) as input_stream:
        for line in input_stream:
            if line.strip():
                d[line.strip()].add(filename)

for key, value in d.items():
    if len(value) > 1:
        print(key, value)
