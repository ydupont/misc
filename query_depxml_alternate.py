"""Query a depxml corpus with dependency patterns.
"""

import itertools
import multiprocessing
import sys
from mweu.to_trie import to_trie
import mweu.depxml as depxml
import mweu.dpath as dpath


def process(args):
    sentence, queries = args
    results = []
    for nth, query in enumerate(queries, 1):
        for nodes in dpath.find_nodes(query, sentence):
            results.append((nth, sorted(
                [node for node in nodes],
                key=lambda x: [int(i) for i in x.cluster.split("_")[1:]]
            )))
    return results


def main(
    pattern_file,
    inputfilenames,
    column=2,
    limit=0,
    context_value="context",
    number_of_processors=1
):
    tries = []
    queries = []
    n = limit
    for line in pattern_file:
        if limit > 0 and n == 0:
            break
        n -= 1
        try:
            tree_dep = line.strip().split('\t')[column]
            tokens = tree_dep.split()[1:-1]
            query = dpath.Query(trie=to_trie(tokens), leaf="_")
            if len(query) > 1:
                queries.append(query)
                tries.append(tree_dep)
        except IndexError as ie:
            print(
                "cannot process ({ie}): {tree_dep}".format(ie=ie, tree_dep=tree_dep),
                file=sys.stderr
            )

    pool = (
        multiprocessing.Pool(processes=number_of_processors)
        if number_of_processors > 1
        else None
    )
    for inputfilename in inputfilenames:
        sentences = depxml.read_targz(inputfilename)
        for sentence in sentences:
            print(sentence.name)
            for edge in sentence.edges:
                print(
                    sentence.id2node[edge.source],
                    "--",
                    edge.label,
                    "->",
                    sentence.id2node[edge.target]
                )
            print()
        for query in queries:
            print(query.trie)
        print()
        input_list = list(zip(
            sentences,
            itertools.cycle([queries])
        ))
        results = (
            pool.map(process, input_list)
            if pool is not None
            else map(process, input_list)
        )
        for res in results:
            for nth, result in res:
                relevant = [node for node in result if node.context != "+"]
                print(inputfilename, end='\t')
                print(tries[nth - 1], end='\t')
                print(" ".join([node.form for node in result]), end='\t')
                print(" ".join([node.form for node in relevant]), end='\t')
                print(" ".join([node.lemma for node in relevant]), end='\t')
                print(" ".join([" ".join(node.fids) for node in relevant]))

        query1 = dpath.nodes("nc")
        query2 = dpath.edges("N2", "prep")
        nodes1 = dpath.find_nodes(query1, sentences[0])
        nodes2 = dpath.find_nodes(query2, sentences[0])
        print(nodes1)
        print(nodes2)


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "pattern_file",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The path to the pattern file."
    )
    parser.add_argument(
        "inputfilenames",
        nargs="+",
        help="The path to the input files (tar.gz files with .dis.dep.xml files)."
    )
    parser.add_argument(
        "-c", "--column",
        type=int,
        default=-2,
        help="The column to read in pattern file (default: %(default)s)"
    )
    parser.add_argument(
        "--context-value",
        default="context",
        help="The context identifier in node features (default: %(default)s)"
    )
    parser.add_argument(
        "--limit",
        type=int,
        default=0,
        help="The maximum number of patterns to read (default: %(default)s)"
    )
    parser.add_argument(
        "-n", "--number-of-processors",
        type=int,
        default=1,
        help="The number of processors to use (default: %(default)s)"
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
