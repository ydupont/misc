"""Find fids in contextualized+pattern file."""

import argparse
import sys
import collections


def main(input_stream, contextfile):
    mwes = []
    all_fids = set()
    fids2mwe = {}
    for line in input_stream:
        if line.strip().startswith("#"):
            continue
        if line.strip().startswith("%"):
            continue
        if "xxx" in line:
            continue
        parts = line.strip().split("\t")
        if len(parts) == 2:
            mwes.append(parts)
            all_fids.add(parts[1])
            fids2mwe[parts[1]] = line.strip()

    found_fids = set()
    with open(contextfile, "r", encoding="utf-8") as fd, \
        open("new.txt", "w", encoding="utf-8") as new:
        for line in fd:
            parts = line.strip().split("\t")
            if not parts:
                continue
            fids = parts[1]
            if fids in all_fids:
                found_fids.add(fids)
                print(line.strip())
            else:
                new.write(line)
    errors = [fids2mwe[unfound] for unfound in all_fids - found_fids]
    for unfound in sorted(errors):
        print(unfound, file=sys.stderr)
    print("{} fids not found over {} fids".format(len(sorted(all_fids - found_fids)), len(all_fids)), file=sys.stderr)


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The MWEs to find (MWE + fids)."
    )
    parser.add_argument(
        "contextfile",
        help="The file with MWEs (MWE + fids + contextualized + patterns)."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
