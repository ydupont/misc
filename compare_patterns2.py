"""Check elements in common between patterns.
"""

import argparse
import collections
import sys


def main(left, right, lc, rc):
    count_l = collections.Counter()
    with open(left) as input_stream:
        for line in input_stream:
            count_l[line.strip().split("\t")[lc].strip()] += 1

    count_r = collections.Counter()
    with open(right) as input_stream:
        for line in input_stream:
            count_r[line.strip().split("\t")[rc].strip()] += 1

    common = set(count_l.keys()) & set(count_r.keys())
    print(len(common), len(common) / len(count_r))
    s = sum(x[1] for x in count_r.most_common())
    s1 = sum(count_r[x] for x in common)
    print(s, s1, s1 / s)

    xdata = []
    ydata = []
    acc = 0.0
    total_r = sum(count_r.values())
    nth = 0
    for item, count in count_l.most_common():
        nth += 1
        acc += count_r.get(item, 0)
        xdata.append(nth)
        ydata.append(acc / total_r)
    ydata = ydata[::-1]

    import matplotlib.pyplot as plt

    plt.plot(xdata, ydata)
    plt.show()

def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "left",
        help="Reference file."
    )
    parser.add_argument(
        "right",
        help="Comparison file."
    )
    parser.add_argument(
        "--lc",
        type=int, default=3,
        help="Pattern column for left (default: %(default)s)."
    )
    parser.add_argument(
        "--rc",
        type=int, default=1,
        help="Pattern column for right (default: %(default)s)."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
