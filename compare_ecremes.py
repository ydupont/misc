"""Check elements in common between patterns.
"""

import argparse
import sys


def main(ecremes, comparison, output_file):
    set1 = set()
    set2 = set()
    with open(ecremes) as input_stream:
        for line in input_stream:
            parts = line.strip().split("\t")
            set1.add(parts[2])
    with open(comparison) as input_stream:
        for line in input_stream:
            parts = line.strip().split("\t")
            set2.add(parts[-2])
    set3 = set1 & set2
    set1 -= set3
    set2 -= set3
    print("#" + "=" * 32)
    print("# " + "only ecremes:")
    print("#" + "=" * 32)
    for item in sorted(set1):
        print(item)
    print()
    print("#" + "=" * 32)
    print("# " + "only new:")
    print("#" + "=" * 32)
    for item in sorted(set2):
        print(item)
    print()
    print("#" + "=" * 32)
    print("# " + "both:")
    print("#" + "=" * 32)
    for item in sorted(set3):
        print(item)
    print()
    print("only ecremes:  ", len(set1))
    print("only extracted:", len(set2))
    print("both:          ", len(set3))


def parse_cl(argv=None):
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "ecremes",
        help="Input file."
    )
    parser.add_argument(
        "comparison",
        help="Input file."
    )
    parser.add_argument(
        "output_file",
        nargs="?",
        type=argparse.FileType("w"), default=sys.stdout,
        help="Output file."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
