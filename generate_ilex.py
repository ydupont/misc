"""Generate intentional LeFFF entries given MWE examples.
"""

# colonnes :
# lemme	morphologie?	{poids=100};Lemma;{category};{arbre_syntaxique};{synt_head=$0};%default
#
# verbes avec possessif :
#     attraper la lune avec Poss0 dents	0+	100;Lemma;cf;<Suj:cln|sn>;synt_head=$0;%default
#     expédier Poss0 poing dans la figure	0+	100;Lemma;cf;<Suj:cln|sn,Objde:de-sn|en>;synt_head=$0;%default
#     faire bien Poss0 âge	0+	100;Lemma;cf;<Suj:cln|sn>;synt_head=$0;%default
#     jeter Poss0 poids dans la balance	0+	100;Lemma;cf;<Suj:cln|sn>;synt_head=$0;%default
#     trouver chaussure à Poss0 pied	0+	100;Lemma;cf;<Suj:cln|sn>;synt_head=$0;%default
#     vendre Poss0 vie chèrement	0+	100;Lemma;cf;<Suj:cln|sn>;synt_head=$0;%default
#
# quand verbe pas premier token :
#     se {retourner} toute la nuit dans son lit	0+0+inv+inv+inv+inv+det-son+inv	100;Lemma;cf;<Suj:cln|sn>;@pron,synt_head=$0;%default

import sys


def get_info(cat):
    if cat == "v":
        return {
            "category": "cf",
            "syntax": "<Suj:cln|sn>",
            "category_name": "synt_head",
            "additional": "",
            "morphology": "",
            "morphosyntax": ""
        }
    elif cat == "n":
        return {
            "category": "nc",
            "syntax": "<Objde:(de-sinf|de-sn),Objà:(à-sinf)>",
            "category_name": "cat",
            "additional": "nc,@?",
            "morphology": "?",
            "morphosyntax": "nc-?"
        }


def find_lv(parts):
    for i, part in enumerate(parts):
        if part in ("se",):
            continue
        return i


def get_syntax(parts, cat):
    new_parts = parts[:]
    suj = "Suj:(cln|scompl|sinf|sn)"
    obj = ""
    obl = ""
    if parts[-1] == "à":
        obj = "|Objà:(à-sn)"
        del new_parts[-1]
    elif parts[-1] == "de":
        obj = "|Objde:(de-sn)"
        del new_parts[-1]
    elif parts[-1] in ("avec", "dans", "en", "sur", "pour", "contre"):
        obj = "|Obl:({}-sn)".format(parts[-1])
        del new_parts[-1]
    return "<{}{}{}>".format(suj, obj, obl), new_parts


def main(input_stream, output_stream, output_format="ilex"):
    formats = {
        'ilex': '',
        'nompred':
            '{entry}'
            '\tinv'
            '\t{weight}'
            ';Lemma'
            ';{category}'
            ';{syntax}'
            '{additional}'
            ';lightverb={predicate}'
            ';%default\n'
    }
    fmt = formats[output_format]

    data = []
    for line in input_stream:
        parts = line.strip().split("\t")
        examples = tuple(part.replace("' ", "'") for part in parts[-1].split(" / "))
        data.append(tuple([parts[-2], examples]))
    data = sorted(set(data))

    for tree, lemmas in data:
        mwe_cat = tree.split()[1]
        for lemma in lemmas:
            parts = lemma.split()
            syntax, parts = get_syntax(parts, "v")
            if len(parts) == 1:
                continue
            if output_format == "ilex":
                lv_index = find_lv(parts)
                lightverb = parts[lv_index]
                if lv_index != 0:
                    parts[lv_index] = '{'+parts[lv_index]+'}'
                output_stream.write(
                    '{entry}'
                    '\t{flex}'
                    '\t{weight}'
                    ';Lemma'
                    ';{category}'
                    ';{syntax}'
                    ';lightverb={lightverb}'
                    ';%default\n'
                    .format(
                        entry=" ".join(parts),
                        flex="0+",
                        weight=100,
                        category="cf",  # TODO: cf is only for verbs, adapt to nc, etc.
                        predicate="{full}_____{nth_sense}".format(full=" ".join(parts), nth_sense=1),  # adapt to multiple senses
                        syntax="<Suj:cln|sn>",
                        lightverb=lightverb
                    )
                )
            if output_format == "nompred":
                output_stream.write(
                    '{entry}'
                    '\tinv'
                    '\t{weight}'
                    ';Lemma'
                    ';{category}'
                    ';{syntax}'
                    '{additional}'
                    ';lightverb={predicate}'
                    ';%default\n'.format(
                        entry=" ".join(parts[1:]),
                        weight=100,
                        category="cf",  # TODO: cf is only for verbs, adapt to nc, etc.
                        syntax=syntax,
                        additional="",
                        category_name="synt_head",
                        predicate=parts[0]
                    )
                )


def parse_cl(argv=None):
    import argparse
    
    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "input_stream",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="Input file."
    )
    parser.add_argument(
        "output_stream",
        nargs="?",
        type=argparse.FileType("w"), default=sys.stdout,
        help="Output file."
    )
    parser.add_argument(
        "-f", "--output_format",
        choices=("ilex", "nompred"),
        default="ilex",
        help="The output format for lexicon entries (default: %(default)s)."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
