"""Transform a list of depxml files (stored in tar.gz) into a single text file that will be one
sentence per line and tokens separated by a space.
"""

import sys
import tarfile
import itertools
import lxml.etree as ET
import mweu.depxml as depxml


def process(clusters):
    forms = list(set(itertools.chain.from_iterable(c.forms for c in clusters)))
    try:
        forms.sort(key=lambda x: int(x.split('|', 1)[0].split('F')[1]))
    except IndexError:
        print(forms, file=sys.stderr)
        raise
    return [f.split('|', 1)[1] for f in forms]


def main(inputfilename, what):
    sentences = []
    with tarfile.open(inputfilename, "r:gz") as tar:
        members = [m for m in tar.getmembers() if m.name.endswith(".dis.dep.xml")]
        for member in members:
            fd = tar.extractfile(member)
            try:
                tree = ET.fromstring(fd.read())
                sentences.append(
                    (
                        member.name,
                        sorted(
                            [depxml.Cluster(**cluster.attrib) for cluster in tree.iterfind("cluster")],
                            key=lambda x: (x.left, -x.right)
                        )
                    )
                )
            except ET.XMLSyntaxError:
                pass
            except ValueError:
                print("cannot process {}".format(member.name), file=sys.stderr)
                raise
    for name, sentence in sentences:
        text = " ".join(process(sentence))
        if what in text:
            print(name, text)


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.replace("\n", " "))
    parser.add_argument(
        "inputfilename",
        help="The path to the input files (tar.gz files with .dis.dep.xml files)."
    )
    parser.add_argument(
        "what",
        help="What to find"
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
