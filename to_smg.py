"""Transform a MWE dependency pattern list to dpath requests.
"""

import sys
import collections


cat2class = {
    # frmg
    "adj": "adjective",
    "adjPref": "adjective",
    "adv": "adverb",
    "advPref": "adverb",
    'advneg': 'adverb',
    "aux": "auxilliary",
    'clg': 'clitic',
    'cll': 'clitic',
    'clneg': 'clitic',
    "cla": "clitic",
    "cld": "clitic",
    "cln": "clitic",
    "clr": "clitic",
    'csu': 'clitic',
    'ilimp': 'clitic',
    "coo": "conjuction",
    'det': 'det',
    "N": "cnoun_leaf",
    'n': 'cnoun_leaf',
    'nc': 'cnoun_leaf',
    'ncpred': 'cnoun_leaf',
    "np": "cnoun_leaf",
    'N2': 'cnoun_leaf',
    'v': 'verb',
    'VMod': 'verb',
    'prep': 'prep',
    "predet": "pronoun",
    "prel": "pronoun",
    "pres": "pronoun",
    "pri": "pronoun",
    "pro": "pronoun",

    "CS": "?",
    "Infl": "?",
    "S": "?",
    "_": "?",
    "ce": "?",
    "comp": "?",
    "incise": "?",
    "number": "?",
    "que": "?",
    "que_restr": "?",
    "strace": "?",
    "supermod": "?",
    "unknown": "?",
    "xpro": "?",

    # PARSEME
    "ADJ": "adjective",
    "ADV": "adverb",
    "AUX": "auxilliary",
    "CCONJ": "conjunction",
    "DET": "det",
    "NOUN": "noun",
    "PRON": "pronoun",
    "PROPN": "noun",
    "VERB": "verb",

    "NUM": "cardinal?!",
    "SCONJ": "?!",
    "ADP": "adposition?!",
    "PART": "?!",
}


frmg_classes = set([
    "categories",
    "simple_categories",
    "adj",
    "adj_argument",
    "_adj_as_modifier",
    "adj_as_modifier",
    "adj_on_noun",
    "adj_before_noun",
    "adj_before_noun_alt",
    "adj_after_noun",
    "adj_range",
    "adj_on_adj",
    "adj_before_ncpred",
    "adj_on_s",
    "adj_on_s_subject_mod",
    "adj_before_s",
    "adj_after_vmod",
    "special_adj_after_s",
    "adj_after_s",
    "adj_after_s_as_adv",
    "adj_as_comp",
    "adj_as_leaf",
    "adj_in_acomp",
    "det",
    "_predet",
    "simple_predet",
    "cnoun_as_predet",
    "predet",
    "predet_on_det",
    "predet_on_pro",
    "number_as_det",
    "adj_on_det",
    "unsat_noun",
    "noun",
    "_pnoun",
    "pnoun",
    "pronoun",
    "_partitive_de",
    "partitive_noun",
    "_partitive_pronoun",
    "partitive_pro",
    "partitive_pri",
    "partitive_relative",
    "ce_pronoun",
    "whpronoun",
    "_pronoun_as_mod",
    "pronoun_as_mod",
    "pronoun_as_mod_on_s",
    "pronoun_as_mod_on_adj",
    "gender_alternative",
    "gender_alternative_cln",
    "gender_alternative_pro",
    "gender_alternative_pri",
    "pronoun_as_mod_on_coo",
    "_cnoun",
    "_sat_cnoun",
    "cnoun",
    "cnoun_leaf",
    "cnoun_as_noun_mod_name",
    "cnoun_as_quantity_mod",
    "cnoun_as_quantity_mod_on_supermod",
    "cnoun_as_quantity_mod_on_prep",
    "cnoun_as_quantity_mod_on_csu",
    "cnoun_as_quantity_mod_intensive_adv",
    "cnoun_as_mod_on_pronoun",
    "cnoun_as_position_mod",
    "bodypart_cnoun_as_modifier",
    "N2_as_comp",
    "cnoun_as_adv",
    "_cnoun_as_adv_on_s",
    "cnoun_as_adv_on_s",
    "cnoun_as_adv_on_vmod",
    "cnoun_as_adv_on_coo",
    "cnoun_as_adv_on_N2",
    "cnoun_as_adv_mod",
    "cnoun_mod_on_countable_noun",
    "person_on_s",
    "audience_on_s",
    "reference_on_s",
    "position_on_s",
    "dislocated_on_s",
    "dislocated_on_s_ante",
    "dislocated_on_s_post",
    "ce_rel_on_s",
    "paren_on_s",
    "address_on_s",
    "cnoun_as_comp",
    "pnoun_as_cnoun",
    "adj_as_cnoun",
    "noun_apposition",
    "noun_apposition_incise",
    "noun_apposition_noincise",
    "noun_apposition_np",
    "xpro_on_noun",
    "refmark_on_N2",
    "word_affix",
    "word_prefix",
    "adj_prefix",
    "adj_prefix_on_nc",
    "adj_prefix_on_np",
    "adj_prefix_on_adj",
    "adv_prefix",
    "adv_prefix_on_adj",
    "adv_prefix_on_v",
    "adv_prefix_on_adv",
    "word_suffix",
    "adj_suffix",
    "adj_suffix_on_nc",
    "adj_suffix_on_np",
    "_verb_or_aux",
    "_v_with_subcat",
    "v_with_subcat",
    "aux_with_subcat",
    "adj_with_subcat",
    "_verb",
    "verb",
    "_verb_canonical",
    "verb_canonical",
    "verb_canonical_xcomp_by_adj",
    "verb_ilya_as_time_mod",
    "verb_voici_as_time_mod",
    "verb_oblige_as_mod",
    "_verb_extraction",
    "verb_extraction",
    "verb_extraction_relative",
    "verb_extraction_wh",
    "verb_extraction_cleft",
    "verb_extraction_topic",
    "verb_extraction_topic_active",
    "verb_extraction_topic_passive",
    "verb_mod_extraction",
    "verb_mod_extraction_wh",
    "wh_sentence",
    "verb_mod_extraction_cleft",
    "_special_cleft_extraction",
    "special_cleft_extraction_post",
    "special_cleft_extraction_post2",
    "special_cleft_extraction_ante",
    "special_cleft_extraction_wh",
    "verb_mod_extraction_relative",
    "noun_mod_relative",
    "_rel_modifier",
    "noun_rel_modifier",
    "extraposed_rel_modifier",
    "advpro_rel_modifier",
    "relnom_as_noun",
    "adj_rel_modifier",
    # "prep_as_comp",
    "prep",
    "prep_argument",
    "prep_in_comp",
    "_prep_modifier",
    "prep_modifier",
    "prep_pri_modifier",
    "prep_noun_modifier",
    "prep_v_modifier",
    "prep_adj_modifier",
    "prep_pres_modifier",
    "prep_PP_modifier",
    "_prep_s_modifier",
    "prep_s_modifier",
    "prep_s_modifier_ante_no_coma",
    "prep_vmod_modifier",
    "prep_coo_modifier",
    "prep_vmodprep_modifier",
    "whpro_modifier",
    "whpro_modifier_on_S",
    "whpro_modifier_on_vmod",
    "prep_locutive_as_modifier",
    "prep_locutive_as_prep_modifier",
    "prep_locutive_as_csu_modifier",
    "csu",
    "csu_arg_or_subst",
    "_csu_s_modifier",
    "csu_s_modifier",
    "csu_vmod_modifier",
    "csu_other_modifier",
    "csu_N2_modifier",
    "csu_adjP_modifier",
    "consessive_relatives",
    "adv",
    "adv_leaf",
    "adv_modifier",
    "adv_subcat",
    "adv_v",
    "adv_before_v",
    "adv_adv",
    "adv_pro",
    "adv_advpro",
    "adv_number",
    "adv_on_predet",
    "adv_ncpred",
    "adv_advneg",
    "adv_adj",
    "adv_ante_adj",
    "adv_post_adj",
    "adv_comp",
    "adv_csu",
    "adv_N2",
    "adv_timemod",
    "adv_s",
    "adv_PP",
    "quantity_adv_N2",
    "adj_comparative",
    "adj_comparative_intro_N2",
    "adj_comparative_intro_PP",
    "adv_alt_comparative",
    "adv_alt_comparative_on_adv",
    "adv_alt_comparative_on_advneg",
    "adv_alt_comparative_on_adj",
    "adv_alt_comparative_on_N2",
    "advneg_as_modifier",
    "advneg_on_predet",
    "advneg_on_adj",
    "_pres_s",
    "pres_on_s",
    "pres_on_coo",
    "participiale",
    "subordonate",
    "participiale_on_noun",
    "_participiale_on_S",
    "participiale_on_S",
    "simple_participiale_after_S",
    "incise_participiale_on_S",
    "participiale_on_vmod",
    "participiale_on_coo",
    "_spunct",
    "_spunct_unsat",
    "empty_spunct",
    "spunct",
    "spunct_sat",
    "spunct_unsat",
    "extra_end_material",
    "spunct_xdot",
    "spunct_others",
    "spunct_wh",
    "spunct_semicoma",
    "sep_sentence_punct",
    "sep_sentence_punct1",
    "sep_sentence_punct_coma",
    "sep_sentence_punct_sat",
    "sbound_sep",
    "quoted_block",
    "quoted_sentence",
    "quoted_N2",
    "quoted_N",
    "quoted",
    "double_quoted",
    "simple_quoted",
    "chevron_quoted",
    "plus_quoted",
    "quoted_as_N2",
    "quoted_sentence_as_mod",
    "quoted_sentence_as_ante_mod",
    "quoted_sentence_as_post_mod",
    "spunct_in_par",
    "spunct_in_par_on_N2",
    "spunct_in_par_on_adj",
    "spunct_in_par_on_adv",
    "spunct_in_par_on_v",
    "interjection",
    "adv_interjection",
    "pres_interjection",
    "N2_interjection",
    "sentence_starter",
    "lex_starter",
    "coord_starter",
    "refmark",
    "refmark_starter",
    "unsatN2_starter",
    "coord",
    "coord_ante_ni",
    "coord_ante_advneg",
    "coord_no_ni",
    "N2_coord",
    "N2_coord_agr",
    "S_coord",
    "csu_coord",
    "supermod_coord",
    "PP_coord",
    "PP_or_CS_coord_block",
    "ArgComp_coord",
    "ArgComp_coord_block",
    "adj_coord",
    "title_coord",
    "adjP_coord",
    "adjP_coord_block",
    "time_coord",
    "simple_coord",
    "adv_coord",
    "advneg_coord",
    "np_coord",
    "nc_coord",
    "prep_coord",
    "number_coord",
    "det_coord",
    "predet_coord",
    "v_coord",
    "V1_coord",
    "mod_on_coo",
    "mod_on_prep",
    "adv_mod_on_coo",
    "advneg_mod_on_coo",
    "pp_mod_on_coo",
    "csu_mod_on_coo",
    "ou_non",
    "ou_non_on_S",
    "ou_non_on_vmod",
    "ou_non_on_aux",
    "ou_non_on_CS",
    "ou_non_on_adjP",
    "enum",
    "coord_block",
    "simple_enum",
    "N2_enum",
    "adjP_enum",
    "ante_adj_enum",
    "S_enum",
    "clneg",
    # "clitics",
    "clitic_sequence",
    "exclude",
    "advneg",
    "advneg_verb",
    "modal_verb",
    "comparer",
    "superlative_as_mod",
    "superlative_as_adv_mod",
    "superlative_as_adj_mod",
    "mod_on_superlative",
    "mod_on_superlative_possible_modifier",
    "mod_on_superlative_csu_modifier",
    "comparative_as_mod",
    "comparative_as_wrapping_mod",
    "comparer_ante_position",
    "comparer_post_position",
    "comparative_as_adv_mod",
    "comparative_as_adj_mod",
    "comparative_as_anteadj_mod",
    "comparative_as_PP_mod",
    "comparative_as_N2_mod",
    "comparative_as_CS_mod",
    "comparative_as_v_mod",
    "comparative_as_vmod_mod",
    "mod_on_comparative",
    "mod_on_comparative_modifier",
    "mod_on_comparative_simple_modifier",
    "mod_on_comparative_adj_modifier",
    "mod_on_comparative_adv_modifier",
    "mod_on_comparative_N2_modifier",
    "mod_on_comparative_PP_modifier",
    "mod_on_comparative_S_modifier",
    "mod_on_comparative_CS_modifier",
    "mod_on_quantity_modifier",
    "mod_on_quantity_modifier_CS",
    "light_categories",
    "_aux_verbs",
    "aux_verbs",
    "aux_acomp_verbs",
    "aux_acomp_verbs1",
    "aux_acomp_verbs2",
    "cleft_verb",
    "verb_categorization",
    "verb_categorization_active",
    "verb_categorization_passive",
    "verb_argument",
    "verb_argument_subject",
    "verb_argument_other",
    "_collect_real_subject",
    "collect_real_subject",
    "collect_real_clefted_subject",
    "impersonal_subject",
    "clitic_inverted_subject",
    "true_subject",
    "real_group_comp",
    "real_group_clefted_comp",
    "real_group_topic_comp",
    "real_arg_topic_scomp",
    "collect_real_arg",
    "_real_arg_xcomp",
    "real_arg_xcomp",
    "real_arg_by_adj",
    "real_arg_xcomp_by_adj",
    "real_arg_xcomp_canonical",
    "real_special_arg_ncpred",
    "agreement",
    "full_agreement",
    "node_agreement",
    "verb_agreement",
    "verb_agreement_ancestor",
    "auxiliary",
    "deep_auxiliary",
    # "shallow_auxiliary",
    "modifier_at_S_level",
    "modifier_on_S",
    "modifier_before_S",
    "modifier_after_S",
    "modifier_on_vmod",
    "modifier_post_vmod",
    "modifier_postsubj_vmod",
    "modifier_on_coo",
    "modifier_on_prep",
    "mod_on_N2",
    "modifier_on_N2",
    "modifier_on_x",
    "modifier_before_x",
    # "modifier_after_x",
    "short_sentence",
    "comp_sentence",
    "adv_sentence",
    "advneg_sentence",
    "pres_sentence",
    "gerundive_sentence",
    "N2_sentence",
    "PP_sentence",
    "csu_sentence",
    "S_pseudo_passive_sentence",
    "adj_with_subcat_short_sentence",
    "S_pseudo_adj_sentence",
    "que_as_advneg",
])


superclasses = {
    'ArgComp_coord': {'coord'},
    'ArgComp_coord_block': {'_real_arg_xcomp'},
    'N2_coord': {'N2_coord_agr', 'coord'},
    'N2_enum': {'N2_coord_agr', 'simple_enum'},
    'N2_interjection': {'interjection'},
    'N2_sentence': {'short_sentence'},
    'PP_coord': {'coord'},
    'PP_sentence': {'short_sentence'},
    'S_coord': {'coord'},
    'S_enum': {'simple_enum'},
    'S_pseudo_adj_sentence': {'_verb_canonical'},
    'S_pseudo_passive_sentence': {'_verb_canonical'},
    'V1_coord': {'coord'},
    '_adj_as_modifier': {'adj'},
    '_aux_verbs': {'clitic_inverted_subject', 'light_categories', 'Root'},
    '_cnoun': {'noun'},
    '_cnoun_as_adv_on_s': {'cnoun_as_adv'},
    '_csu_s_modifier': {'subordonate', 'csu'},
    '_participiale_on_S': {'participiale'},
    '_partitive_pronoun': {'_pnoun', '_partitive_de'},
    '_pnoun': {'noun'},
    '_prep_modifier': {'prep'},
    '_pres_s': {'simple_categories'},
    '_pronoun_as_mod': {'_pnoun'},
    '_sat_cnoun': {'_cnoun'},
    '_spunct_unsat': {'_spunct'},
    '_verb': {'_verb_or_aux'},
    '_verb_canonical': {'verb'},
    '_verb_extraction': {'verb'},
    '_verb_or_aux': {'categories', 'clitic_inverted_subject'},
    'address_on_s': {'light_categories'},
    'adj': {'categories'},
    'adjP_coord': {'coord'},
    'adjP_enum': {'enum'},
    'adj_after_noun': {'adj_on_noun', 'adj_range'},
    'adj_after_s': {'adj_on_s_subject_mod'},
    'adj_after_s_as_adv': {'adj_on_s'},
    'adj_after_vmod': {'adj_on_s_subject_mod'},
    'adj_as_cnoun': {'_sat_cnoun'},
    'adj_as_comp': {'adj', 'adj_range'},
    'adj_as_leaf': {'adj'},
    'adj_as_modifier': {'_adj_as_modifier'},
    'adj_before_ncpred': {'adj_as_modifier'},
    'adj_before_noun': {'adj_on_noun'},
    'adj_before_noun_alt': {'adj'},
    'adj_before_s': {'adj_on_s_subject_mod'},
    'adj_comparative': {'adj_on_noun'},
    'adj_comparative_intro_N2': {'adj_comparative'},
    'adj_comparative_intro_PP': {'adj_comparative'},
    'adj_coord': {'coord'},
    'adj_on_adj': {'_adj_as_modifier', 'adj_range'},
    'adj_on_det': {'simple_categories'},
    'adj_on_noun': {'adj_as_modifier'},
    'adj_on_s': {'adj_as_modifier'},
    'adj_on_s_subject_mod': {'adj_on_s'},
    'adj_prefix': {'word_prefix'},
    'adj_prefix_on_adj': {'adj_prefix'},
    'adj_prefix_on_nc': {'adj_prefix'},
    'adj_prefix_on_np': {'adj_prefix'},
    'adj_suffix': {'word_suffix'},
    'adj_suffix_on_nc': {'adj_suffix'},
    'adj_suffix_on_np': {'adj_suffix'},
    'adv': {'categories'},
    'adv_N2': {'adv_modifier'},
    'adv_PP': {'adv_modifier'},
    'adv_adj': {'adv_modifier'},
    'adv_adv': {'adv_modifier'},
    'adv_advneg': {'adv_modifier'},
    'adv_advpro': {'adv_modifier'},
    'adv_alt_comparative': {'adv_modifier'},
    'adv_alt_comparative_on_N2': {'adv_alt_comparative'},
    'adv_alt_comparative_on_adj': {'adv_alt_comparative'},
    'adv_alt_comparative_on_adv': {'adv_alt_comparative'},
    'adv_alt_comparative_on_advneg': {'adv_alt_comparative'},
    'adv_ante_adj': {'adv_adj'},
    'adv_before_v': {'adv_modifier'},
    'adv_comp': {'adv_modifier'},
    'adv_coord': {'simple_coord'},
    'adv_csu': {'adv_modifier'},
    'adv_interjection': {'interjection'},
    'adv_leaf': {'adv'},
    'adv_mod_on_coo': {'adv', 'mod_on_coo'},
    'adv_modifier': {'adv'},
    'adv_ncpred': {'adv_modifier'},
    'adv_number': {'adv_modifier'},
    'adv_on_predet': {'adv_modifier'},
    'adv_post_adj': {'adv_adj'},
    'adv_prefix': {'word_prefix'},
    'adv_prefix_on_adj': {'adv_prefix'},
    'adv_prefix_on_adv': {'adv_prefix'},
    'adv_prefix_on_v': {'adv_prefix'},
    'adv_pro': {'adv_modifier'},
    'adv_s': {'adv'},
    'adv_sentence': {'short_sentence'},
    'adv_timemod': {'adv_modifier'},
    'adv_v': {'adv_modifier'},
    'advneg': {'simple_categories'},
    'advneg_as_modifier': {'light_categories'},
    'advneg_coord': {'coord'},
    'advneg_mod_on_coo': {'categories', 'mod_on_coo'},
    'advneg_on_adj': {'advneg_as_modifier'},
    'advneg_on_predet': {'advneg_as_modifier'},
    'advneg_sentence': {'short_sentence'},
    'advneg_verb': {'advneg'},
    'advpro_rel_modifier': {'_rel_modifier'},
    'aux_acomp_verbs': {'_aux_verbs'},
    'aux_acomp_verbs1': {'aux_acomp_verbs'},
    'aux_acomp_verbs2': {'aux_acomp_verbs'},
    'aux_verbs': {'_aux_verbs'},
    'aux_with_subcat': {'_v_with_subcat'},
    'bodypart_cnoun_as_modifier': {'cnoun'},
    'ce_pronoun': {'_pnoun'},
    'chevron_quoted': {'quoted'},
    'cleft_verb': {'_verb_or_aux', 'clneg'},
    'clitics': {'clneg'},
    'cnoun': {'_cnoun'},
    'cnoun_as_adv_mod': {'simple_categories'},
    'cnoun_as_adv_on_N2': {'cnoun_as_adv'},
    'cnoun_as_adv_on_coo': {'cnoun_as_adv'},
    'cnoun_as_adv_on_s': {'_cnoun_as_adv_on_s'},
    'cnoun_as_adv_on_vmod': {'_cnoun_as_adv_on_s'},
    'cnoun_as_comp': {'simple_categories'},
    'cnoun_as_mod_on_pronoun': {'cnoun'},
    'cnoun_as_noun_mod_name': {'cnoun'},
    'cnoun_as_position_mod': {'cnoun'},
    'cnoun_as_predet': {'cnoun'},
    'cnoun_as_quantity_mod': {'cnoun'},
    'cnoun_as_quantity_mod_intensive_adv': {'cnoun_as_quantity_mod'},
    'cnoun_as_quantity_mod_on_csu': {'cnoun_as_quantity_mod'},
    'cnoun_as_quantity_mod_on_prep': {'cnoun_as_quantity_mod'},
    'cnoun_as_quantity_mod_on_supermod': {'cnoun_as_quantity_mod'},
    'cnoun_leaf': {'cnoun'},
    'cnoun_mod_on_countable_noun': {'cnoun'},
    'collect_real_arg': {'real_arg_xcomp_canonical'},
    'collect_real_clefted_subject': {'_collect_real_subject'},
    'collect_real_subject': {'impersonal_subject', '_collect_real_subject'},
    'comp_sentence': {'short_sentence'},
    'comparative_as_CS_mod': {'comparative_as_wrapping_mod'},
    'comparative_as_N2_mod': {'comparative_as_wrapping_mod'},
    'comparative_as_PP_mod': {'comparative_as_wrapping_mod'},
    'comparative_as_adj_mod': {'comparative_as_wrapping_mod'},
    'comparative_as_adv_mod': {'comparative_as_wrapping_mod'},
    'comparative_as_anteadj_mod': {'comparative_as_wrapping_mod'},
    'comparative_as_v_mod': {'comparative_as_wrapping_mod'},
    'comparative_as_vmod_mod': {'Incise', 'comparative_as_mod'},
    'comparative_as_wrapping_mod': {'comparative_as_mod'},
    'comparer': {'adv'},
    'coord': {'simple_categories'},
    'coord_starter': {'sentence_starter'},
    'csu': {'simple_categories'},
    'csu_N2_modifier': {'csu_other_modifier'},
    'csu_adjP_modifier': {'csu_other_modifier'},
    'csu_arg_or_subst': {'csu'},
    'csu_coord': {'coord'},
    'csu_mod_on_coo': {'mod_on_coo'},
    'csu_other_modifier': {'subordonate', 'csu'},
    'csu_s_modifier': {'_csu_s_modifier'},
    'csu_sentence': {'short_sentence'},
    'csu_vmod_modifier': {'_csu_s_modifier'},
    'deep_auxiliary': {'auxiliary'},
    'det': {'simple_categories'},
    'det_coord': {'simple_coord'},
    'dislocated_on_s_ante': {'dislocated_on_s'},
    'dislocated_on_s_post': {'dislocated_on_s'},
    'double_quoted': {'quoted'},
    'empty_spunct': {'_spunct_unsat'},
    'extraposed_rel_modifier': {'_rel_modifier'},
    'gender_alternative_cln': {'gender_alternative'},
    'gender_alternative_pri': {'gender_alternative'},
    'gender_alternative_pro': {'gender_alternative'},
    'gerundive_sentence': {'subordonate', 'short_sentence'},
    'incise_participiale_on_S': {'participiale_on_S'},
    'lex_starter': {'sentence_starter'},
    'mod_on_comparative': {'mod_on_superlative'},
    'mod_on_comparative_CS_modifier': {'mod_on_comparative_modifier'},
    'mod_on_comparative_N2_modifier': {'mod_on_comparative_modifier'},
    'mod_on_comparative_PP_modifier': {'mod_on_comparative_modifier'},
    'mod_on_comparative_S_modifier': {'mod_on_comparative_modifier'},
    'mod_on_comparative_adj_modifier': {'mod_on_comparative_simple_modifier'},
    'mod_on_comparative_adv_modifier': {'mod_on_comparative_modifier'},
    'mod_on_comparative_simple_modifier': {'mod_on_comparative_modifier'},
    'mod_on_quantity_modifier': {'prep', 'mod_on_superlative'},
    'mod_on_quantity_modifier_CS': {'csu', 'mod_on_superlative'},
    'mod_on_superlative_csu_modifier': {'mod_on_superlative'},
    'mod_on_superlative_possible_modifier': {'mod_on_superlative'},
    'modal_verb': {'_verb', 'Foot'},
    'modifier_after_S': {'modifier_on_S'},
    'modifier_after_x': {'modifier_on_x'},
    'modifier_at_S_level': {'Incise'},
    'modifier_before_S': {'modifier_on_S'},
    'modifier_before_x': {'modifier_on_x'},
    'modifier_post_vmod': {'modifier_on_vmod'},
    'modifier_postsubj_vmod': {'modifier_on_vmod'},
    'nc_coord': {'simple_coord'},
    'noun': {'simple_categories'},
    'noun_apposition_incise': {'noun_apposition'},
    'noun_apposition_noincise': {'noun_apposition'},
    'noun_apposition_np': {'noun_apposition', 'simple_categories'},
    'noun_rel_modifier': {'_rel_modifier'},
    'np_coord': {'simple_coord'},
    'number_as_det': {'simple_categories'},
    'number_coord': {'simple_coord'},
    'ou_non_on_CS': {'ou_non'},
    'ou_non_on_S': {'ou_non'},
    'ou_non_on_adjP': {'ou_non'},
    'ou_non_on_aux': {'ou_non'},
    'ou_non_on_vmod': {'ou_non'},
    'participiale': {'subordonate'},
    'participiale_on_S': {'_participiale_on_S'},
    'participiale_on_coo': {'_participiale_on_S'},
    'participiale_on_noun': {'participiale'},
    'participiale_on_vmod': {'_participiale_on_S'},
    'partitive_noun': {'_cnoun', '_partitive_de'},
    'partitive_pri': {'_partitive_pronoun'},
    'partitive_pro': {'_partitive_pronoun'},
    'plus_quoted': {'quoted'},
    'pnoun': {'_pnoun'},
    'pnoun_as_cnoun': {'_sat_cnoun'},
    'pp_mod_on_coo': {'mod_on_coo'},
    'predet': {'_predet'},
    'predet_coord': {'simple_coord'},
    'predet_on_det': {'_predet'},
    'predet_on_pro': {'_predet'},
    'prep': {'simple_categories'},
    'prep_PP_modifier': {'_prep_modifier'},
    'prep_adj_modifier': {'prep_modifier'},
    'prep_argument': {'prep'},
    'prep_as_comp': {'simple_categories'},
    'prep_coo_modifier': {'_prep_s_modifier'},
    'prep_coord': {'simple_coord'},
    'prep_in_comp': {'prep'},
    'prep_locutive_as_csu_modifier': {'prep_locutive_as_modifier'},
    'prep_locutive_as_modifier': {'simple_categories'},
    'prep_locutive_as_prep_modifier': {'prep_locutive_as_modifier'},
    'prep_modifier': {'_prep_modifier'},
    'prep_noun_modifier': {'prep_modifier'},
    'prep_pres_modifier': {'prep_modifier'},
    'prep_pri_modifier': {'prep_modifier'},
    'prep_s_modifier': {'_prep_s_modifier'},
    'prep_s_modifier_ante_no_coma': {'_prep_s_modifier'},
    'prep_v_modifier': {'prep_modifier'},
    'prep_vmod_modifier': {'_prep_s_modifier'},
    'prep_vmodprep_modifier': {'_prep_s_modifier'},
    'pres_interjection': {'interjection'},
    'pres_on_coo': {'_pres_s'},
    'pres_on_s': {'_pres_s'},
    'pres_sentence': {'short_sentence'},
    'pronoun': {'_pnoun'},
    'pronoun_as_mod': {'_pronoun_as_mod'},
    'pronoun_as_mod_on_adj': {'_pronoun_as_mod'},
    'pronoun_as_mod_on_coo': {'pronoun_as_mod'},
    'pronoun_as_mod_on_s': {'pronoun_as_mod'},
    'quantity_adv_N2': {'adv_modifier'},
    'quoted_N': {'quoted_block'},
    'quoted_N2': {'quoted_block'},
    'quoted_sentence': {'quoted_block'},
    'quoted_sentence_as_ante_mod': {'quoted_sentence_as_mod'},
    'quoted_sentence_as_post_mod': {'quoted_sentence_as_mod'},
    'real_arg_xcomp': {'_real_arg_xcomp'},
    'real_arg_xcomp_by_adj': {'real_arg_by_adj', 'real_arg_xcomp'},
    'real_arg_xcomp_canonical': {'real_arg_xcomp'},
    'real_group_comp': {'ArgGroup'},
    'refmark_on_N2': {'refmark'},
    'refmark_starter': {'sentence_starter', 'refmark'},
    'sbound_sep': {'sep_sentence_punct_sat'},
    'sep_sentence_punct': {'spunct_unsat'},
    'sep_sentence_punct1': {'sep_sentence_punct'},
    'sep_sentence_punct_coma': {'sep_sentence_punct'},
    'sep_sentence_punct_sat': {'spunct_sat'},
    'shallow_auxiliary': {'auxiliary'},
    'simple_categories': {'categories'},
    'simple_coord': {'coord'},
    'simple_enum': {'enum'},
    'simple_participiale_after_S': {'participiale_on_S'},
    'simple_predet': {'simple_categories'},
    'simple_quoted': {'quoted'},
    'special_adj_after_s': {'adj_on_s_subject_mod'},
    'special_cleft_extraction_ante': {'_special_cleft_extraction'},
    'special_cleft_extraction_post': {'_special_cleft_extraction'},
    'special_cleft_extraction_post2': {'_special_cleft_extraction'},
    'special_cleft_extraction_wh': {'_special_cleft_extraction'},
    'spunct': {'_spunct'},
    'spunct_in_par_on_N2': {'value(N2)'},
    'spunct_in_par_on_adj': {'value(adj)'},
    'spunct_in_par_on_adv': {'value(advneg)'},
    'spunct_in_par_on_v': {'value(v)'},
    'spunct_others': {'spunct_unsat', 'extra_end_material'},
    'spunct_sat': {'spunct'},
    'spunct_semicoma': {'spunct_unsat', 'extra_end_material'},
    'spunct_unsat': {'spunct'},
    'spunct_wh': {'wh_sentence', 'spunct_unsat', 'extra_end_material'},
    'spunct_xdot': {'spunct_unsat', 'extra_end_material'},
    'superlative_as_adj_mod': {'superlative_as_mod'},
    'superlative_as_adv_mod': {'superlative_as_mod'},
    'supermod_coord': {'coord'},
    'time_coord': {'coord'},
    'title_coord': {'coord'},
    'unsatN2_starter': {'sentence_starter'},
    'unsat_noun': {'categories'},
    'v_coord': {'simple_coord'},
    'v_with_subcat': {'_v_with_subcat'},
    'verb': {'_verb'},
    'verb_argument_other': {'verb_argument'},
    'verb_argument_subject': {'verb_argument'},
    'verb_canonical': {'_verb_canonical'},
    'verb_canonical_xcomp_by_adj': {'_verb_canonical'},
    'verb_categorization_active': {'verb_categorization', 'real_special_arg_ncpred'},
    'verb_categorization_passive': {'verb_categorization'},
    'verb_extraction': {'_verb_extraction'},
    'verb_extraction_cleft': {'_verb_extraction'},
    'verb_extraction_relative': {'verb_extraction'},
    'verb_extraction_topic': {'_verb_extraction'},
    'verb_extraction_topic_active': {'verb_extraction_topic'},
    'verb_extraction_topic_passive': {'verb_extraction_topic'},
    'verb_extraction_wh': {'wh_sentence', 'verb_extraction'},
    'verb_ilya_as_time_mod': {'_verb_canonical'},
    'verb_mod_extraction_cleft': {'verb_mod_extraction', 'XGroup'},
    'verb_mod_extraction_relative': {'verb_mod_extraction'},
    'verb_mod_extraction_wh': {'verb_mod_extraction', 'wh_sentence'},
    'verb_oblige_as_mod': {'categories'},
    'verb_voici_as_time_mod': {'_verb_canonical'},
    'whpro_modifier': {'simple_categories'},
    'whpro_modifier_on_S': {'whpro_modifier'},
    'whpro_modifier_on_vmod': {'whpro_modifier'},
    'whpronoun': {'_pnoun'},
    'word_affix': {'simple_categories'},
    'word_prefix': {'word_affix'},
    'word_suffix': {'word_affix'},
    'xpro_on_noun': {'simple_categories'},
}


def find_common_ancestor(*names):
    """Find common ancestor(s) to a set of class names."""

    def find_common_ancestor_rec(lists, ancestors_list):
        ancestors = {item for item in set.intersection(*ancestors_list) if not item.startswith('_')}
        if ancestors:
            return ancestors

        new_lists = []
        all_empty = True
        for i, lst in enumerate(lists):
            new_l = []
            for cls in lst:
                sclasses = superclasses.get(cls, set())
                ancestors_list[i] |= sclasses
                new_l.extend(list(sclasses))
            new_lists.append(new_l)
            if new_l:
                all_empty = False
        if all_empty:
            return set()
        return find_common_ancestor_rec(new_lists, ancestors_list)

    return find_common_ancestor_rec([[name] for name in names], [set([name]) for name in names])


def to_tree(tokens):
    """Transform tokens to a tree structure than can be parsed more easily."""

    def add_indices(tokens):
        lst = []
        i = 0
        node2tree = {}
        for token in tokens:
            if token[0] not in "()":
                node = "{token}@{i}".format(token=token, i=i)
                lst.append(node)
                node2tree[node] = "T_{token}_{i}".format(token=token, i=i)
                i += 1
            else:
                lst.append(token)
        return lst, node2tree

    stack = []
    toks, node2tree = add_indices(tokens)
    toks = toks[::-1]
    val = toks.pop()
    d = {"({val},)".format(val=val): {}}
    tmp = list(d.values())[0]
    path = [list(d.keys())[0]]
    for i in range(len(toks)):
        item = toks.pop()
        if item.startswith('('):
            item = item[1:]
        stack.append(item)
        if '@' in item:
            cat = stack.pop()
            edge = stack.pop()
            key = "({cat},{edge})".format(cat=cat, edge=edge)
            tmp[key] = {}
            tmp = tmp[key]
            path.append(key)
        elif ')' == item:
            path.pop()
            tmp = d
            for node in path:
                tmp = tmp[node]
    return d, node2tree


def put(dct, node2tree, context_value, class_name, nth, tree_dep, commons, out_fd=sys.stdout):
    """Put the dpath query in out_fd."""

    def put_rec(subd, from_key):
        root_node = from_key[1: from_key.index(',')]
        for key in sorted(subd.keys(), key=lambda x: int(x[x.index("@") + 1: x.index(',')])):
            node = key[1: key.index(',')]
            edge = key[key.index(',') + 1: -1]
            edge = edge.replace(context_value, "")
            out_fd.write("    substitute({top}/node({edge})) = {bot};\n".format(
                top=node2tree[root_node],
                edge=edge,
                bot=node2tree[node]
            ))
            put_rec(subd[key], key)

    key = list(dct.keys())[0]
    out_fd.write("class {}_{} {{\n".format(class_name, nth))
    out_fd.write("    % {nth}. {tree_dep}\n".format(nth=nth, tree_dep=tree_dep))
    for node, tree in sorted(node2tree.items(), key=lambda x: int(x[0].split("@")[1])):
        cat, index = node.split("@")
        index = int(index)
        classes = [item for item in commons[index] if item.startswith("class=")]
        if classes:
            for cls in sorted(classes):
                out_fd.write(
                    "    tree {tree} <: {cls};\n".format(
                        tree=tree,
                        cls=cls[cls.index("=") + 1:]
                    )
                )
        else:
            cls = cat2class.get(
                node[:node.index('@')],
                cat2class[node[:node.index('@')].split('|')[0]]
            )
            comment = (
                " % ERROR: could not find FRMG class for this node"
                if cls == "?"
                else " % WARNING: no common FRMG class found, guessing from POS"
            )
            out_fd.write(
                "    tree {tree} <: {cls};{comment}\n"
                .format(tree=tree, cls=cls, comment=comment)
            )
    put_rec(dct[key], key)
    out_fd.write("}")


def find_common(input_file, base_pattern_column=-3, pattern_column=-2, find_superclasses=False):
    """Find, for each MWE in input_file, common elements between its occurrences.
    A common element is either an element that is found in each and every MWE occurrence
    or an frmg_class that superclasses every class found for a token.
    """

    base2pats = collections.defaultdict(set)
    pat_counts = collections.Counter()
    for line in input_file:
        parts = line.strip().split("\t")
        base_pattern = parts[base_pattern_column]
        pattern = parts[pattern_column]
        base2pats[base_pattern].add(pattern)
        pat_counts[base_pattern] += 1
    commons = {}
    all_classes = {}
    for key in base2pats:
        commons[key] = [None for _ in key.split()]
        all_classes[key] = [set() for _ in key.split()]
        for item in base2pats[key]:
            parts = item.split()
            for i, part in enumerate(parts):
                if part[0] in '()':
                    continue
                features = part.split("|")
                class_feats = []
                tree_feats = []
                for feat in features[1][5:].split(";"):
                    if feat in frmg_classes:
                        class_feats.append("class={}".format(feat))
                    else:
                        tree_feats.append("tree={}".format(feat))
                features = [features[0]] + class_feats + tree_feats + features[2:]
                all_classes[key][i] |= set(class_feats)
                if commons[key][i] is None:
                    commons[key][i] = set(features)
                else:
                    commons[key][i] &= set(features)
    # checking if we have any common class feature.
    # if not, we try and check if we find some common parent class.
    if find_superclasses:
        for key in commons:
            print(key, file=sys.stderr)
            for i, features in enumerate(commons[key]):
                if features is None:
                    continue
                if not [item for item in features if item.startswith("class=")]:
                    lst = [item.split("=", 1)[1] for item in all_classes[key][i]]
                    if lst:
                        classes = find_common_ancestor(
                            *lst
                        )
                        commons[key][i] |= set("class={}".format(cls) for cls in classes)
                    else:
                        print("\t", features, all_classes[key][i], file=sys.stderr)
    return base2pats, pat_counts, commons


def main(
    inputfile,
    outputfile,
    base_pattern_column=3,
    pattern_column=-1,
    context_value="::context",
    class_name="mwe",
    find_superclasses=False
):
    base2pats, pat_counts, commons = find_common(inputfile, base_pattern_column, pattern_column, find_superclasses=find_superclasses)
    for nth, key in enumerate(sorted(commons, key=lambda x: (-pat_counts[x], x)), 1):
        dct, node2tree = to_tree(key.split()[1: -1])
        only_common = [item for item in commons[key] if item is not None]
        put(dct, node2tree, context_value, class_name, nth, key, only_common, out_fd=outputfile)
        outputfile.write("\n\n")


def parse_cl(argv=None):
    import argparse

    parser = argparse.ArgumentParser(description=__doc__.split("\n")[0])
    parser.add_argument(
        "inputfile",
        nargs="?",
        type=argparse.FileType("r"), default=sys.stdin,
        help="The path to the input file (list of MWEs occurrences with patterns)."
    )
    parser.add_argument(
        "outputfile",
        nargs="?",
        type=argparse.FileType("w"), default=sys.stdout,
        help="The path to the output file."
    )
    parser.add_argument(
        "-b", "--base-pattern-column",
        type=int, default=-3,
        help="Column for base pattern, that is with no context and features (default: %(default)s)"
    )
    parser.add_argument(
        "-p", "--pattern-column",
        type=int, default=-2,
        help="Column for pattern information (default: %(default)s)"
    )
    parser.add_argument(
        "-c", "--context-value",
        default="::context",
        help="The context marquer (default: %(default)s)"
    )
    parser.add_argument(
        "-n", "--class-name",
        default="mwe",
        help="The name to use for classes (default: %(default)s)"
    )
    parser.add_argument(
        "-s", "--find-superclasses",
        action="store_true",
        help="When no common class is found between MWE patterns, "
             "find a superclass that covers them all."
    )
    args = parser.parse_args(argv)

    main(**vars(args))


if __name__ == "__main__":
    parse_cl()
    sys.exit(0)
