from remove_features2 import remove_features, remove_context


def test(tree):
    tree_parts = tree.split()
    tree_parts = [
        part.split("|")
        for part in tree_parts
    ]

test("(mwe nc|tree=n:agreement;nc:agreement;cnoun_leaf|hum=-|sat=+|time=-|wh=-|enum=enum (N2::context coo|tree=N2_coord;coord_no_ni;shallow_auxiliary|hum=-|sat=+|time=-|wh=-|enum=- ) (det::context det|tree=det|dem=-|det=+|poss=-|wh=-|gender=masc|number=sg|numberposs=-|def=+ ) (N2 adj|tree=adj_after_noun;arg0:adj_argument;arg1:adj_argument;adj:agreement;modifier_after_x;adjP:node_agreement;shallow_auxiliary|number=sg|gender=masc ) )")
test("(mwe nc|tree=n:agreement;nc:agreement;cnoun_leaf|hum=-|sat=-|time=-|wh=-|enum=- (nc coo|tree=coord_ante_ni;nc_coord;shallow_auxiliary|hum=-|time=- (ni coo|tree=lexical ) (coord3 nc|tree=lexical ) ) )")
